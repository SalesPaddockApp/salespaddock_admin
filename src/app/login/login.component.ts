import { Component, ViewEncapsulation } from '@angular/core';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { RouterModule, Router }   from '@angular/router';
import { AuthService } from '../sharedServices/authService/auth.service';
@Component({
  selector: 'login',
  encapsulation: ViewEncapsulation.Emulated,
  styles: [require('./login.scss')],
  template: require('./login.html'),
})
export class Login {

  public form: FormGroup;

  constructor(private fb: FormBuilder, private authService: AuthService, private _router: Router) {
    this.form = this.fb.group({
      'email': ['', Validators.compose([Validators.required])],
      'password': ['', Validators.compose([Validators.required])]
    });
  }

  signIn(form) {
    this.authService.login(form.value)
      .subscribe((res) => {
        if (res.errCode == 0) {
          localStorage.setItem('user', res.response.token);
          
          this._router.navigate(['pages']);
          return res.code;
        }
        else{
          alert(res.Message);
        }
      })
}


}
