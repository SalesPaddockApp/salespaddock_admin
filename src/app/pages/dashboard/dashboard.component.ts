import {Component, ViewEncapsulation} from '@angular/core';
import { AuthService } from '../../sharedServices/authService/auth.service';

@Component({
  selector: 'dashboard',
  encapsulation: ViewEncapsulation.None,
  styles: [require('./dashboard.scss')],
  template: require('./dashboard.html')
})
export class Dashboard {

  constructor(
        private authService: AuthService) {
  }
  ngOnInit(){
    this.authService.checkCredentials();
  }

}
