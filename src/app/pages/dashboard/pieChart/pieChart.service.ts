import { Injectable } from '@angular/core';
import { BaThemeConfigProvider, colorHelper } from '../../../theme';

import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class PieChartService {
  private Url: string;
  private headers = new Headers({
    'Content-Type': 'application/json',
    "authorization": 'Basic ' + btoa("horse" + ":" + "123456"),
    "x-access-token": localStorage.getItem("user")
  })
  private options = new RequestOptions({
    headers: this.headers
  })

  private pieColor = this._baConfig.get().colors.custom.dashboardPieChart;

  constructor(private _baConfig: BaThemeConfigProvider, private _http: Http) {
  }

  getUsersCount() {
    let url = API_URL+":2020/getDashBoardData";
    let options = new RequestOptions({ headers: this.headers });
    let body = JSON.stringify({});
    return this._http.post(url, body, options)
      .map(res => res.json())
      .map((res) => {
        let obj = [{
          color: this.pieColor,
          description: 'Total Users',
          stats: res.response.totalUsers,
          icon: 'ion-android-people fa-4x',
        },
        {
          color: this.pieColor,
          description: 'Users this year',
          stats: res.response.usersThisYear,
          icon: 'ion-android-people fa-4x',
        },
        {
          color: this.pieColor,
          description: 'Users this month',
          stats: res.response.usersThisMonth,
          icon: 'ion-android-people fa-4x',
        },
        {
          color: this.pieColor,
          description: 'Total Users today',
          stats: res.response.usersToday,
          icon: 'ion-android-people fa-4x',
        }];
        return obj;
      });
  }
}
