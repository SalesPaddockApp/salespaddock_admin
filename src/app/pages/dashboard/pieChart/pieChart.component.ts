import {Component, ViewEncapsulation} from '@angular/core';

import {PieChartService} from './pieChart.service';

import './pieChart.loader.ts';
import { SocketService } from '../../../sharedServices/socketService/socket.service';

@Component({
  selector: 'pie-chart',
  encapsulation: ViewEncapsulation.None,
  styles: [require('./pieChart.scss')],
  template: require('./pieChart.html')
})
// TODO: move easypiechart to component
export class PieChart 
{

    public charts: any = [];
    private _init = false;
    private subscription: any = null;

  constructor(private _pieChartService: PieChartService) {
  }

  ngOnInit() {
  this._pieChartService.getUsersCount()
    .subscribe((result) => {
      this.charts = result;

    });

  // this.subscription = this._socketService.newUser$.subscribe((data: any) => {
  //   for (var i = 0, len = this.charts.length; i < len; i++) {
  //     if (this.charts[i].description == 'Total Users') {
  //       this.charts[i].stats += 1;
  //       this._loadPieCharts();
  //     }
  //   }
  // });
  //
  // this._socketService.getNewText();
  // this.subscription = this._socketService.newTextMess$.subscribe((data: any) => {
  //
  //   console.log("it is coming here:  " + JSON.stringify(data));
  //
  //   for (var i = 0, len = this.charts.length; i < len; i++) {
  //       if(data.type == 0)
  //       {
  //           if (this.charts[i].description == 'Total text') {
  //             this.charts[i].stats += 1;
  //             this._loadPieCharts();
  //           }
  //       }
  //       else if(data.type == 1)
  //       {
  //           if (this.charts[i].description == 'Total image') {
  //             this.charts[i].stats += 1;
  //             this._loadPieCharts();
  //           }
  //       }
  //       else if(data.type == 2)
  //       {
  //           if (this.charts[i].description == 'Total video') {
  //             this.charts[i].stats += 1;
  //             this._loadPieCharts();
  //           }
  //       }
  //       else if(data.type == 3)
  //       {
  //           if (this.charts[i].description == 'Total location') {
  //             this.charts[i].stats += 1;
  //             this._loadPieCharts();
  //           }
  //       }
  //       else if(data.type == 4)
  //       {
  //           if (this.charts[i].description == 'Total contact') {
  //             this.charts[i].stats += 1;
  //             this._loadPieCharts();
  //           }
  //       }
  //       else if(data.type == 5)
  //       {
  //           if (this.charts[i].description == 'Total audio') {
  //             this.charts[i].stats += 1;
  //             this._loadPieCharts();
  //           }
  //       }
  //   }
  // });
}

  ngAfterViewInit() {
    if (!this._init) {
      this._loadPieCharts();
      this._updatePieCharts();
      this._init = true;
    }
  }

  private _loadPieCharts() {

    jQuery('.chart').each(function () {
      let chart = jQuery(this);
      chart.easyPieChart({
        easing: 'easeOutBounce',
        onStep: function (from, to, percent) {
          jQuery(this.el).find('.percent').text(Math.round(percent));
        },
        barColor: jQuery(this).attr('data-rel'),
        trackColor: 'rgba(0,0,0,0)',
        size: 84,
        scaleLength: 0,
        animation: 2000,
        lineWidth: 9,
        lineCap: 'round',
      });
    });
  }

  private _updatePieCharts() {
    let getRandomArbitrary = (min, max) => { return Math.random() * (max - min) + min; };

    jQuery('.pie-charts .chart').each(function(index, chart) {
      jQuery(chart).data('easyPieChart').update(getRandomArbitrary(55, 90));
    });
  }
}
