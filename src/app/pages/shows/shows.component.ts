import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { BaThemeSpinner, BaThemePreloader } from '../../theme/services';
import { Location } from '@angular/common';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import { ShowsService } from './shows.service';
import { AuthService } from '../../sharedServices/authService/auth.service';

@Component({
  selector: 'shows',
  encapsulation: ViewEncapsulation.None,
  template: `
    <router-outlet></router-outlet>
    `,
  styles: [require('./shows.scss')],
})
export class Shows{
  private addShowForm: FormGroup;
  private editShowForm: FormGroup;
  private showById: any;
  shows: any;
  searchControl = new FormControl();
  constructor(private _location: Location, private showsService: ShowsService, private fb: FormBuilder,
    private authService: AuthService, private router: Router) {
  }
}
