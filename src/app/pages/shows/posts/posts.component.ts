import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { BaThemeSpinner, BaThemePreloader } from '../../theme/services';
import { Location } from '@angular/common';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import { PostsService } from '../../posts/posts.service';
import { AuthService } from '../../../sharedServices/authService/auth.service';
import { ShowsService } from '../shows.service';

@Component({
  selector: 'posts',
  encapsulation: ViewEncapsulation.None,
  template: require('./posts.html')
})
export class Posts implements OnInit {
  private addPostForm: FormGroup;
  private editpostForm: FormGroup;
  private postById: any;
  private postByIdToView: any;
  posts: any;
  private nodata: boolean = false;
  private user: any;
  private userIds: any[] = [];
  searchControl = new FormControl();
  private id: any;
  constructor(private _location: Location, private postsService: PostsService, private fb: FormBuilder,
    private authService: AuthService, private router: Router,
    private _route: ActivatedRoute, private showsService: ShowsService) {
  }

  ngOnInit() {
    this.authService.checkCredentials();
    this._route.params.forEach((params: Params) => {
      this.id = params['id'];
    });
    this.showsService.getParticipantsById(this.id)
      .subscribe((res) => {
        console.log(res)
        if (res.errCode == 0) {
          this.nodata = false
          this.posts = res.response.data;
        }
        else {
          this.posts = null
          this.nodata = true
        }
      });
  }

  getUserById(userId) {
    this.postsService.getUserById(userId)
      .subscribe((data) => {
        console.log(data)
        this.user = data.response.data;
      })
  }
  getPostByIdToView(post) {
    this.postsService.getPostByIdToView(post)
      .subscribe((data) => {
        this.postByIdToView = data.response.data
        console.log(this.postByIdToView)
      })
  }
  goBack(): void {
    this._location.back();
  }
}
