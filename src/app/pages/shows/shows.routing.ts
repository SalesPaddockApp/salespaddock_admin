import { Routes, RouterModule } from '@angular/router';

import { Shows } from './shows.component';
import { CreateShowComponent } from './createshow/createshow.component'
import { EditShowComponent } from './editshow/editshow.component';
import {Active} from './active/active.component';
import {Upcoming} from './upcoming/upcoming.component';
import {InActive} from './inactive/inactive.component';
import {Completed} from './completed/completed.component';
import {Posts} from './posts/posts.component';

const routes: Routes = [
  {
    path: '',
    component: Shows,
    children: [
      { path: '', redirectTo: 'active', pathMatch: 'full' },
      { path: 'active', component: Active },
      { path: 'upcoming', component: Upcoming },
      { path: 'archived', component: Completed },
      { path: 'inactive', component: InActive }
    ]
  },
  { path: 'create', component: CreateShowComponent },
  { path: 'edit/:id', component: EditShowComponent },
  { path: 'posts/:id', component: Posts}
];

export const routing = RouterModule.forChild(routes);
