import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgaModule } from '../../theme/nga.module';
import { ReactiveFormsModule } from '@angular/forms';
import { routing }       from './shows.routing';
import { Shows } from './shows.component';
import { ShowsService } from './shows.service';
import { CreateShowComponent } from './createshow/createshow.component';
import { EditShowComponent } from './editshow/editshow.component';
import { PagesModule } from '../pages.module';
import {Active} from './active/active.component';
import {InActive} from './inactive/inactive.component';
import {Upcoming} from './upcoming/upcoming.component';
import {Completed} from './completed/completed.component';
import {Posts} from './posts/posts.component';
import { PostsService } from '../posts/posts.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgaModule,
    routing,
    ReactiveFormsModule,
    PagesModule
  ],
  declarations: [
    Shows,
    CreateShowComponent,
    EditShowComponent,
    Active,
    InActive,
    Completed,
    Posts,
    Upcoming
  ],
  providers: [
    ShowsService,
    PostsService
  ]
})
export default class ShowsModule {}
