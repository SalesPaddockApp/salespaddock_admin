import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { BaThemeSpinner, BaThemePreloader } from '../../theme/services';
import { Location } from '@angular/common';
import { Router, ActivatedRoute, Params, NavigationExtras } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import { ShowsService } from '../shows.service';
import { AuthService } from '../../../sharedServices/authService/auth.service';

@Component({
  selector: 'active',
  encapsulation: ViewEncapsulation.None,
  template: require('./active.html')
})
export class Active implements OnInit {
  private addShowForm: FormGroup;
  private editShowForm: FormGroup;
  private showById: any;
  shows: any;
  searchControl = new FormControl();
  private participants: any;
  private noparticipants: boolean = false;
  private nodata: boolean = false;
  private ids: any[] = [];

  private moreData: any[];
  private pageNum: number = 0;
  private disable = false;
  private nouserresult: any;

  constructor(private _location: Location, private showsService: ShowsService, private fb: FormBuilder,
    private authService: AuthService, private router: Router) {
  }

  ngOnInit() {
    this.authService.checkCredentials();
    this.getActiveShows();
    this.searchControl.valueChanges
      .debounceTime(400)
      .distinctUntilChanged()
      .flatMap(seachControl => this.showsService.searchActiveShows(seachControl))
      .subscribe(data => {
        if (data.errCode == 0) {
          this.shows = data.response.data
          this.disable = true
          this.nouserresult = false
        }
        else if (data.errNum == 132) {
          this.pageNum = 0;
          this.nouserresult = false
          this.showsService.getActiveShows(this.pageNum)
            .subscribe((res) => {
              if (res.errCode == 0) {
                this.nodata = false
                this.disable = false
                this.shows = res.response.data;
                if (this.shows.length % 10 != 0)
                  this.disable = true;
              }
              else {
                this.nodata = true
              }
            });
        }
        else {
          this.shows = null;
          this.nouserresult = true
        }
      });
    this.initAddShowForm()
  }
  loadMore() {
    this.pageNum = this.pageNum + 1;
    this.showsService.getActiveShows(this.pageNum)
      .subscribe(res => {
        console.log(res)
        this.moreData = res.response.data;
        if (res.errCode == 0) {
          let len = this.moreData.length;
          for (let i = 0; i < len; i++)
            this.shows.push(this.moreData[i]);
        }
        if (this.shows.length % 10 != 0)
          this.disable = true;
      });
  }
  initAddShowForm() {
    this.addShowForm = this.fb.group({
      "productName": ['', Validators.compose([
        Validators.required])]
    })
  }
  getshowById(show) {
    this.showById = show;
    this.editShowForm = this.fb.group({
      "_id": this.showById._id,
      "productName": this.showById.productName
    })
  }

  getActiveShows() {
    this.showsService.getActiveShows(this.pageNum)
      .subscribe((res) => {
        console.log(res);
        if (res.errCode == 0) {
          this.nodata = false
          this.shows = res.response.data;
          console.log(this.shows.length)
          if (this.shows.length % 10 != 0)
              this.disable = true;
        }
        else {
          this.nodata = true
        }
      });
  }

  addshow() {
    this.showsService.addShow(this.addShowForm.value)
      .subscribe((data) => {
        if (data.errCode == 0) {
          this.initAddShowForm()
          this.showsService.getActiveShows(this.pageNum)
            .subscribe(data => {
              this.shows = data.response.data;
            });
        }
      })
  }

  editShow() {
    this.router.navigate(['/pages/shows/edit', this.ids[0]]);
  }

  deleteShow() {
    this.showsService.deleteShow(this.ids)
      .subscribe((data) => {
        console.log(data)
        if (data.errCode == 0) {
          this.showsService.getActiveShows(this.pageNum)
            .subscribe((res) => {
              console.log(res);
              if (res.errCode == 0) {
                this.nodata = false
                this.shows = res.response.data;
              }
              else {
                this.shows = null
                this.nodata = true
              }
            });
        }
      })
  }
  getParticipants(show) {
    if (show.posts.length) {
      this.router.navigate(['pages/shows/posts', show._id]);
    }
  }

  makeShowInActive() {
    this.showsService.makeShowInactive(this.ids)
      .subscribe((data) => {
        console.log(data)
        if (data.errCode == 0) {
          this.showsService.getActiveShows(this.pageNum)
            .subscribe((res) => {
              console.log(res);
              if (res.errCode == 0) {
                this.nodata = false
                this.shows = res.response.data;
              }
              else {
                this.shows = null
                this.nodata = true
              }
            });
        }
      })
  }

  setIds(e, producttype) {
    if (e.target.checked) {
      this.ids.push(producttype._id)
      this.showById = producttype
    }
    else {
      this.ids.splice(this.ids.indexOf(producttype._id));
    }
    console.log(this.ids)
  }
}
