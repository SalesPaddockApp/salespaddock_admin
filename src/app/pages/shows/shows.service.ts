import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class ShowsService {
    private Url: string;
    private headers = new Headers({
        'Content-Type': 'application/json',
        "authorization": 'Basic ' + btoa("horse" + ":" + "123456"),
        "x-access-token": localStorage.getItem("user")
    })
    private options = new RequestOptions({
        headers: this.headers
    })
    constructor(private http: Http) {

    }


    getActiveShows(p) {
        let body = JSON.stringify({pageNum: p});
        let options = new RequestOptions({ headers: this.headers });
        this.Url = API_URL+":2020/getAllActiveShows";
        return this.http.post(this.Url, body, options)
            .map(res => res.json());
    }
    getInActiveShows(p) {
        let body = JSON.stringify({pageNum: p});
        let options = new RequestOptions({ headers: this.headers });
        this.Url = API_URL+":2020/getAllInactiveShows";
        return this.http.post(this.Url, body, options)
            .map(res => res.json());
    }
    getUpcomingShows(p) {
        let body = JSON.stringify({pageNum: p});
        let options = new RequestOptions({ headers: this.headers });
        this.Url = API_URL+":2020/getUpcomingShows";
        return this.http.post(this.Url, body, options)
            .map(res => res.json());
    }
    getCompletedShows(p) {
        let body = JSON.stringify({pageNum: p});
        let options = new RequestOptions({ headers: this.headers });
        this.Url = API_URL+":2020/getAllCompletedShows";
        return this.http.post(this.Url, body, options)
            .map(res => res.json());
    }
    addShow(ShowToAdd) {
        let body = JSON.stringify(ShowToAdd);
        this.Url = API_URL+":2020/createShow";
        return this.http.post(this.Url, body, this.options)
            .map(res => res.json());
    }

    deleteShow(ShowId) {
        let options = new RequestOptions({ headers: this.headers });
        let body = JSON.stringify({ _ids: ShowId });
        let Url = API_URL+":2020/deleteShow";
        return this.http.post(Url, body, options)
            .map(res => res.json());
    }
    editShow(Show) {
        let options = new RequestOptions({ headers: this.headers });
        let body = JSON.stringify(Show);
        this.Url = API_URL+":2020/editShow";
        return this.http.post(this.Url, body, options)
            .map(res => res.json())

    }
    searchActiveShows(seachControl) {
        let options = new RequestOptions({ headers: this.headers });
        let body = JSON.stringify({ searchP: seachControl });
        this.Url = API_URL+":2020/searchActiveShows";
        return this.http.post(this.Url, body, options)
            .map(res => res.json())
    }
    searchInactiveShows(seachControl) {
        let options = new RequestOptions({ headers: this.headers });
        let body = JSON.stringify({ searchP: seachControl });
        this.Url = API_URL+":2020/searchInactiveShows";
        return this.http.post(this.Url, body, options)
            .map(res => res.json())
    }
    searchUpcomingShows(seachControl) {
        let options = new RequestOptions({ headers: this.headers });
        let body = JSON.stringify({ searchP: seachControl });
        this.Url = API_URL+":2020/searchUpcomingShows";
        return this.http.post(this.Url, body, options)
            .map(res => res.json())
    }
    searchCompletedShows(seachControl) {
        let options = new RequestOptions({ headers: this.headers });
        let body = JSON.stringify({ searchP: seachControl });
        this.Url = API_URL+":2020/searchCompletedShows";
        return this.http.post(this.Url, body, options)
            .map(res => res.json())
    }
    getPostsByProductId(id) {
        let body = JSON.stringify({ productId: id });
        let options = new RequestOptions({ headers: this.headers });
        this.Url = API_URL+":2020/getPostByProductId";
        return this.http.post(this.Url, body, options)
            .map(res => res.json());
    }
    getShowById(id) {
        let body = JSON.stringify({ _id: id });
        this.Url = API_URL+":2020/getShowsListById";
        return this.http.post(this.Url, body, this.options)
            .map(res => res.json());
    }
    getProducttypes() {
        let body = JSON.stringify({});
        let options = new RequestOptions({ headers: this.headers });
        this.Url = API_URL+":2020/getProducttypes";
        return this.http.post(this.Url, body, options)
            .map(res => res.json());
    }
    getParticipants(showId) {
        let body = JSON.stringify({ _id: showId });
        this.Url = API_URL+":2020/getParticipants";
        return this.http.post(this.Url, body, this.options)
            .map(res => res.json());
    }
    makeShowInactive(id) {
        let body = JSON.stringify({ _ids: id });
        this.Url = API_URL+":2020/makeShowInactive";
        return this.http.post(this.Url, body, this.options)
            .map(res => res.json());
    }
    makeShowActive(id) {
        let body = JSON.stringify({ _ids: id });
        this.Url = API_URL+":2020/makeShowActive";
        return this.http.post(this.Url, body, this.options)
            .map(res => res.json());
    }
    getProducttypeById(id) {
        let body = JSON.stringify({ _id: id });
        let options = new RequestOptions({ headers: this.headers });
        this.Url = API_URL+":2020/getProductTypeById";
        return this.http.post(this.Url, body, options)
            .map(res => res.json());
    }
    getParticipantsById(id) {
        let body = JSON.stringify({ _id: id });
        let options = new RequestOptions({ headers: this.headers });
        this.Url = API_URL+":2020/getParticipantsById";
        return this.http.post(this.Url, body, options)
            .map(res => res.json());
    }
}
