import { Component, ViewEncapsulation, Input, ViewChild, OnInit, ElementRef, ViewChildren, AfterViewInit, NgZone, Renderer } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl, FormArray } from '@angular/forms';
import { ShowsService } from '../shows.service';
import { FileUploader } from 'ng2-file-upload';
const URL = API_URL+':8009/upload';
import { AgmCoreModule, MapsAPILoader } from 'angular2-google-maps/core';
import { ImageCropperComponent, CropperSettings } from 'ng2-img-cropper';

@Component({
    selector: 'editshow',
    encapsulation: ViewEncapsulation.None,
    template: require('./editshow.html'),
    styles: [require('./editshow.scss')]
})
export class EditShowComponent {
    private Imgage: any;
    private imageurl: any;
    private show: any;
    private producttypes: any;
    private id: string;
    private editShowForm: FormGroup;
    public latitude: number;
    public longitude: number;
    public searchControl: FormControl;
    public zoom: number;
    private posts: any[] = [];
    private selectedposts: any;
    cropperSettings: CropperSettings;
    private data: any;
    private changeImage: boolean = false;
    private cropper: boolean = false;

    private myDatePickerOptions: any;
    private uploaded: boolean = false;

    @ViewChild("search")
    public searchElementRef: ElementRef;

    constructor(private route: ActivatedRoute,
        private router: Router, private showsService: ShowsService, private fb: FormBuilder, private mapsAPILoader: MapsAPILoader, private _ngZone: NgZone, private _renderer: Renderer, private _location: Location) {

        this.cropperSettings = new CropperSettings();
        this.cropperSettings.width = 810;
        this.cropperSettings.height = 540;
        this.cropperSettings.croppedWidth = 810;
        this.cropperSettings.croppedHeight = 540;
        this.cropperSettings.canvasWidth = 300;
        this.cropperSettings.canvasHeight = 200;
        this.data = {};
        this.myDatePickerOptions = {
            todayBtnTxt: 'Today',
            dateFormat: 'yyyy-mm-dd',
            firstDayOfWeek: 'mo',
            sunHighlight: true,
            height: '34px',
            width: '260px',
            inline: false,
            disableUntil: { year: 1800, month: 8, day: 10 },
            selectionTxtFontSize: '16px'
        };
    }

    ngOnInit() {
        let _self = this;
        this.route.params.forEach((params: Params) => {
            this.id = params['id'];
        });
        this.initEditshowForm()
        this.showsService.getShowById(this.id)
            .subscribe((data) => {
                console.log(data)
                this.show = data.response;
                this.showsService.getPostsByProductId(this.show.productId)
                    .subscribe((data) => {
                        if (data.errCode == 0) {
                            this.posts = data.response.data;
                            let selectedposts: any[] = this.show.posts;
                            this.selectedposts = this.show.posts;
                            let postControl = <FormArray>this.editShowForm.controls['posts']
                            selectedposts.forEach(data => {
                                console.log(data)
                                postControl.push(this.editInitPosts(data))
                                this.posts.forEach(res => {
                                    console.log(data)
                                    console.log(res)
                                    if (data.postId == res._id) {
                                        console.log(this.posts.indexOf(res))
                                        this.posts.splice(this.posts.indexOf(res), 1)
                                    }
                                })

                            })
                        }
                    });

                this.editShowForm.controls["_id"].setValue(this.show._id)
                this.editShowForm.controls["productType"].setValue(this.show.productType)
                this.editShowForm.controls["showName"].setValue(this.show.showName)
                this.editShowForm.controls["startDate"].setValue(this.show.startDate)
                this.editShowForm.controls["endDate"].setValue(this.show.endDate)
                this.editShowForm.controls["venue"].setValue(this.show.venue)
                this.editShowForm.controls["lat"].setValue(this.show.lat)
                this.editShowForm.controls["lng"].setValue(this.show.lng)
                this.editShowForm.controls["imgPath"].setValue(this.show.imgPath)
                this.editShowForm.controls["Description"].setValue(this.show.Description)
                this.editShowForm.controls["imgPath"].setValue(this.show.imgPath)
                this.imageurl = this.show.imgPath
                this.zoom = 20;
                this.latitude = this.show.lat;
                this.longitude = this.show.lng;
            })
        this.uploader.onSuccessItem = function (item: any, response: any, status: number, headers: any) {
            _self.Imgage = JSON.parse(response);
            _self.imageurl = (API_URL+"/" + _self.Imgage.url)
            console.log(_self.imageurl);
            _self.editShowForm.controls['imgPath'].setValue(_self.imageurl)
            return { item, response, status, headers };
        };

        // //set google maps defaults
        // this.zoom = 20;
        // this.latitude = 39.8282;
        // this.longitude = -98.5795;

        //create search FormControl
        this.searchControl = new FormControl();

        //set current position
        this.setCurrentPosition();

        //load Places Autocomplete
        this.mapsAPILoader.load().then(() => {
            let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {

            });
            autocomplete.addListener("place_changed", () => {
                //get the place result
                let place: google.maps.places.PlaceResult = autocomplete.getPlace();
                //set latitude and longitude
                this.latitude = place.geometry.location.lat();
                this.longitude = place.geometry.location.lng();
                this.editShowForm.controls['lat'].setValue(this.latitude)
                this.editShowForm.controls['lng'].setValue(this.longitude)
            });
        });
    }

    public uploader: FileUploader = new FileUploader({ url: URL, autoUpload: true, itemAlias: "photo" });
    public hasBaseDropZoneOver: boolean = false;
    public hasAnotherDropZoneOver: boolean = false;

    public fileOverBase(e: any): void {
        this.hasBaseDropZoneOver = e;
    }

    public fileOverAnother(e: any): void {
        this.hasAnotherDropZoneOver = e;
    }
    checkEvent(e) {
        console.log(e)
    }
    cropped(e, i) {
        console.log(this.data)
    }
    chngImage() {
        this.changeImage = true
        this.cropper = true
    }
    confirmUpload(i) {
        console.log(this.data.image)
        let dataURI = this.data.image;
        function dataURItoBlob(dataURI: string) {
            // convert base64 to raw binary data held in a string
            // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
            var byteString = atob(dataURI.split(',')[1]);

            // separate out the mime component
            var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]

            // write the bytes of the string to an ArrayBuffer
            var ab = new ArrayBuffer(byteString.length);
            var ia = new Uint8Array(ab);
            for (var i = 0; i < byteString.length; i++) {
                ia[i] = byteString.charCodeAt(i);
            }

            // write the ArrayBuffer to a blob, and you're done
            var blob = new Blob([ab], { type: mimeString });
            return blob;

            // Old code
            // var bb = new BlobBuilder();
            // bb.append(ab);
            // return bb.getBlob(mimeString);
        }
        var blob = dataURItoBlob(dataURI);
        var fileName = (Math.random().toString(36).substring(7) + ".png");
        let imgagefile = new File([blob], fileName)
        console.log(blob)
        this.makeFileRequest(URL, [], imgagefile).then((result) => {
            console.log(JSON.stringify(result['url']));
            const url = result['url'];
            const option = this.editShowForm.controls['imgPath']
            option.setValue(API_URL+"/" + url)
            console.log(option)
            this.imageurl = API_URL+"/" + url;
            console.log(this.imageurl)
            this.uploaded = true
        })
    }

    makeFileRequest(url: string, params: Array<string>, files: any) {
        return new Promise((resolve, reject) => {
            var formData: any = new FormData();
            var xhr = new XMLHttpRequest();
            // for (var i = 0; i < files.length; i++) {
            //     formData.append("uploads[]", files[i], files[i].name);
            //     console.log(formData.value)
            // }
            formData.append("photo", files, files.name)
            console.log(formData)
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4) {
                    if (xhr.status == 200) {
                        resolve(JSON.parse(xhr.response));
                    } else {
                        reject(xhr.response);
                    }
                }
            }
            xhr.open("POST", url, true);
            xhr.send(formData);
        });
    }

    private setCurrentPosition() {
        if ("geolocation" in navigator) {
            navigator.geolocation.getCurrentPosition((position) => {
                this.latitude = position.coords.latitude;
                this.longitude = position.coords.longitude;
                this.zoom = 20;
                this.editShowForm.controls['lat'].setValue(this.latitude)
                this.editShowForm.controls['lng'].setValue(this.longitude)
            });
        }
    }
    setEndDate(e) {
        console.log(e)
        const option = this.editShowForm.controls['startDate']
        option.setValue(e.formatted)
    }
    setStartDate(e) {
        console.log(e)
        const option = this.editShowForm.controls['endDate']
        option.setValue(e.formatted)
        console.log(option)
    }
    initEditshowForm() {
        this.editShowForm = this.fb.group({
            "_id": '',
            "productType": [{ value: '', disabled: true }, Validators.compose([
                Validators.required])],
            "showName": ['', Validators.compose([
                Validators.required])],
            "startDate": ['', Validators.compose([
                Validators.required])],
            "endDate": ['', Validators.compose([
                Validators.required])],
            "venue": ['', Validators.compose([
                Validators.required])],
            "lat": ['', Validators.compose([
                Validators.required])],
            "lng": ['', Validators.compose([
                Validators.required])],
            "imgPath": ['', Validators.compose([
                Validators.required])],
            "Description": ['', Validators.compose([
                Validators.required])],
            "posts": this.fb.array([]),
        })
    }
    editInitPosts(post) {
        return this.fb.group({
            'userId': [post.userId, Validators.compose([
                Validators.required])],
            'postId': [post.postId, Validators.compose([
                Validators.required])],
            'postName': [post.postName, Validators.compose([
                Validators.required])]
        })
    }
    initPosts(post) {
        return this.fb.group({
            'userId': [post.userId, Validators.compose([
                Validators.required])],
            'postId': [post._id, Validators.compose([
                Validators.required])],
            'postName': [post.productName, Validators.compose([
                Validators.required])]
        })
    }
    editShow() {
        console.log(this.editShowForm.value)
        this.showsService.editShow(this.editShowForm.value)
            .subscribe((data) => {
                if (data.errCode == 0) {
                    this._location.back();
                }
                else {
                    alert(data.Message);
                }
            })

    }

    setPost(e, post) {
        let postControl = <FormArray>this.editShowForm.controls['posts']
        if (e.target.checked == true) {
            postControl.push(this.initPosts(post))
        }
        else {
            postControl.removeAt(post)
        }
    }

    setEditPost(e, post) {
        console.log(post)
        let postControl = <FormArray>this.editShowForm.controls['posts']
        if (e.target.checked == true) {
            postControl.push(this.editInitPosts(post))
        }
        else {
            postControl.removeAt(post)
        }
    }
}
