import { Component, ViewEncapsulation, Input, ViewChild, OnInit, ElementRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl, FormArray } from '@angular/forms';
import { FileUploader } from 'ng2-file-upload';
import { ShowsService } from '../shows.service';
const URL = API_URL+':8009/upload';
import { AgmCoreModule, MapsAPILoader } from 'angular2-google-maps/core';
import { ImageCropperComponent, CropperSettings } from 'ng2-img-cropper';

@Component({
    selector: 'createshow',
    encapsulation: ViewEncapsulation.None,
    template: require('./createshow.html'),
    styles: [require('./createshow.scss')]
})
export class CreateShowComponent {
    private Imgage: any;
    private imageurl: any;
    private posts: any;
    private msisdn: string;
    private TOP: any;
    private productypes: any;
    private postControl: FormArray;
    private createShowForm: FormGroup;
    public latitude: number;
    public longitude: number;
    public searchControl: FormControl;
    public zoom: number;
    cropperSettings: CropperSettings;
    private data: any;
    private changeImage: boolean = false;
    private cropper: boolean = false;
    private uploaded: boolean = false;

    private myDatePickerOptions: any;
    @ViewChild("search")
    public searchElementRef: ElementRef;

    constructor(private route: ActivatedRoute,
        private router: Router, private showsService: ShowsService, private fb: FormBuilder, private mapsAPILoader: MapsAPILoader) {
        this.cropperSettings = new CropperSettings();
        this.cropperSettings.width = 810;
        this.cropperSettings.height = 540;
        this.cropperSettings.croppedWidth = 810;
        this.cropperSettings.croppedHeight = 540;
        this.cropperSettings.canvasWidth = 300;
        this.cropperSettings.canvasHeight = 200;
        this.data = {};
        this.myDatePickerOptions = {
            todayBtnTxt: 'Today',
            dateFormat: 'yyyy-mm-dd',
            firstDayOfWeek: 'mo',
            sunHighlight: true,
            height: '34px',
            width: '260px',
            inline: false,
            disableUntil: { year: 1800, month: 8, day: 10 },
            selectionTxtFontSize: '16px'
        };
    }

    ngOnInit() {
        let _self = this;
        this.initAddShowForm()
        this.showsService.getProducttypes()
            .subscribe((data) => {
                if (data.errCode == 0) {
                    this.productypes = data.response.data;
                    for (var index = 0; index < this.productypes.length; index++) {
                    if(this.productypes[index].productName == "Horse"){
                        this.getPostsByProductId(this.productypes[index]._id)
                    }
                }
                }
            })
        this.postControl = <FormArray>this.createShowForm.controls['posts']
        this.uploader.onSuccessItem = function (item: any, response: any, status: number, headers: any) {
            _self.Imgage = JSON.parse(response);
            _self.imageurl = (API_URL+"/" + _self.Imgage.url)
            console.log(_self.imageurl);
            alert(_self.Imgage.url);
            _self.createShowForm.controls['imgPath'].setValue(_self.imageurl)
            return { item, response, status, headers };
        };

        //set google maps defaults
        this.zoom = 20;
        this.latitude = 39.8282;
        this.longitude = -98.5795;

        //create search FormControl
        this.searchControl = new FormControl();

        //set current position
        this.setCurrentPosition();

        //load Places Autocomplete
        this.mapsAPILoader.load().then(() => {
            let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {

            });
            autocomplete.addListener("place_changed", () => {
                //get the place result
                let place: google.maps.places.PlaceResult = autocomplete.getPlace();
                //set latitude and longitude
                this.latitude = place.geometry.location.lat();
                this.longitude = place.geometry.location.lng();
                this.createShowForm.controls['lat'].setValue(this.latitude)
                this.createShowForm.controls['lng'].setValue(this.longitude)
            });
        });

    }
    public uploader: FileUploader = new FileUploader({ url: URL, autoUpload: true, itemAlias: "photo" });
    public hasBaseDropZoneOver: boolean = false;
    public hasAnotherDropZoneOver: boolean = false;

    public fileOverBase(e: any): void {
        this.hasBaseDropZoneOver = e;
    }

    public fileOverAnother(e: any): void {
        this.hasAnotherDropZoneOver = e;
    }
    cropped(e, i) {
        console.log(this.data)
    }
    confirmUpload(i) {
        console.log(this.data.image)
        let dataURI = this.data.image;
        function dataURItoBlob(dataURI: string) {
            // convert base64 to raw binary data held in a string
            // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
            var byteString = atob(dataURI.split(',')[1]);

            // separate out the mime component
            var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]

            // write the bytes of the string to an ArrayBuffer
            var ab = new ArrayBuffer(byteString.length);
            var ia = new Uint8Array(ab);
            for (var i = 0; i < byteString.length; i++) {
                ia[i] = byteString.charCodeAt(i);
            }

            // write the ArrayBuffer to a blob, and you're done
            var blob = new Blob([ab], { type: mimeString });
            return blob;

            // Old code
            // var bb = new BlobBuilder();
            // bb.append(ab);
            // return bb.getBlob(mimeString);
        }
        var blob = dataURItoBlob(dataURI);
        var fileName = (Math.random().toString(36).substring(7) + ".png");
        let imgagefile = new File([blob], fileName)
        console.log(blob)
        this.makeFileRequest(URL, [], imgagefile).then((result) => {
            console.log(JSON.stringify(result['url']));
            const url = result['url'];
            const option = this.createShowForm.controls['imgPath']
            option.setValue(API_URL+"/" + url)
            console.log(option)
            this.imageurl = API_URL+"/" + url;
            console.log(this.imageurl)
            this.uploaded = true
        })
    }
    setEndDate(e) {
        console.log(e)
        const option = this.createShowForm.controls['startDate']
        option.setValue(e.formatted)
    }
    setStartDate(e) {
        console.log(e)
        const option = this.createShowForm.controls['startDate']
        option.setValue(e.formatted)
    }
    makeFileRequest(url: string, params: Array<string>, files: any) {
        return new Promise((resolve, reject) => {
            var formData: any = new FormData();
            var xhr = new XMLHttpRequest();
            // for (var i = 0; i < files.length; i++) {
            //     formData.append("uploads[]", files[i], files[i].name);
            //     console.log(formData.value)
            // }
            formData.append("photo", files, files.name)
            console.log(formData)
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4) {
                    if (xhr.status == 200) {
                        resolve(JSON.parse(xhr.response));
                    } else {
                        reject(xhr.response);
                    }
                }
            }
            xhr.open("POST", url, true);
            xhr.send(formData);
        });
    }
    private setCurrentPosition() {
        if ("geolocation" in navigator) {
            navigator.geolocation.getCurrentPosition((position) => {
                this.latitude = position.coords.latitude;
                this.longitude = position.coords.longitude;
                this.zoom = 20;
                this.createShowForm.controls['lat'].setValue(this.latitude)
                this.createShowForm.controls['lng'].setValue(this.longitude)
            });
        }
    }
    check(e) {
        console.log(this.searchElementRef.nativeElement)
    }
    initAddShowForm() {
        this.createShowForm = this.fb.group({
            "productId": ['', Validators.compose([
                Validators.required])],
            "productType": ['', Validators.compose([
                Validators.required])],
            "showName": ['', Validators.compose([
                Validators.required])],
            "startDate": ['', Validators.compose([
                Validators.required])],
            "endDate": ['', Validators.compose([
                Validators.required])],
            "venue": ['', Validators.compose([
                Validators.required])],
            "lat": ['', Validators.compose([
                Validators.required])],
            "lng": ['', Validators.compose([
                Validators.required])],
            "imgPath": [this.imageurl, Validators.compose([
                Validators.required])],
            "Description": ['', Validators.compose([
                Validators.required])],
            "posts": this.fb.array([]),
        })
    }
    initPosts(post) {
        console.log(post)
        return this.fb.group({
            'userId': [post.userId, Validators.compose([])],
            'postId': [post._id, Validators.compose([])],
            'postName': [post.productName, Validators.compose([])]
        })
    }

    setPost(e, post) {
        if (e.target.checked == true) {
            this.postControl.push(this.initPosts(post))
        }
        else {
            this.postControl.removeAt(post)
        }
    }
    createShow() {
        console.log(this.createShowForm.value);
        this.showsService.addShow(this.createShowForm.value)
            .subscribe((data) => {
                if (data.errCode == 0) {
                    this.router.navigate(['pages/shows/inactive'])
                }
                else {
                    alert(data.Message);
                }
            })

    }
    getPostsByProductId(e) {
        console.log(e)
        this.createShowForm.controls["productId"].setValue(e)
        this.showsService.getProducttypeById(e)
            .subscribe((data) => {
                if (data.errCode == 0) {
                    this.createShowForm.controls["productType"].setValue(data.response.data.name)
                }
            });
        this.showsService.getPostsByProductId(e)
            .subscribe((data) => {
                console.log(data)
                if (data.errCode == 0) {
                    this.posts = data.response.data;
                }
            });
    }

}
