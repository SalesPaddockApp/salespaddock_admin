import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class PrefsettingsService {
    private Url: string;
    private headers = new Headers({
        'Content-Type': 'application/json',
        "authorization": 'Basic ' + btoa("horse" + ":" + "123456"),
        "x-access-token": localStorage.getItem("user")
    })
    private options = new RequestOptions({
        headers: this.headers
    })
    constructor(private http: Http) {

    }

    getProducttypeById(id) {
        let body = JSON.stringify({_id: id});
        let options = new RequestOptions({ headers: this.headers });
        this.Url = API_URL+":2020/getProductTypeById";
        return this.http.post(this.Url, body, options)
            .map(res => res.json());
    }
    getPrefsettings(id) {
        let body = JSON.stringify({ productId: id });
        let options = new RequestOptions({ headers: this.headers });
        this.Url = API_URL+":2020/getPrefSettings";
        return this.http.post(this.Url, body, options)
            .map(res => res.json());
    }
    getPrefToCreatePost(id,userId) {
        let body = JSON.stringify({ productId: id, userId : userId});
        let options = new RequestOptions({ headers: this.headers });
        this.Url = API_URL+":2020/getPrefToCreatePost";
        return this.http.post(this.Url, body, options)
            .map(res => res.json());
    }
    addPrefsetting(PrefsettingToAdd) {
        let body = JSON.stringify(PrefsettingToAdd);
        this.Url = API_URL+":2020/createPrefsetting";
        return this.http.post(this.Url, body, this.options)
            .map(res => res.json());
    }

    deletePrefsetting(PrefsettingId, prirority, productType) {
        let options = new RequestOptions({ headers: this.headers });
        let body = JSON.stringify({ _id: PrefsettingId, prirority: prirority, productType: productType });
        let Url = API_URL+":2020/deletePrefSetting";
        return this.http.post(Url, body, options)
            .map(res => res.json());
    }
    editPrefsetting(Prefsetting) {
        let options = new RequestOptions({ headers: this.headers });
        let body = JSON.stringify(Prefsetting);
        this.Url = API_URL+":2020/editPrefSettng";
        return this.http.post(this.Url, body, options)
            .map(res => res.json())

    }
    search(seachControl) {
        let options = new RequestOptions({ headers: this.headers });
        let body = JSON.stringify({ searchP: seachControl });
        this.Url = API_URL+":2020/searchPrefsettings";
        return this.http.post(this.Url, body, options)
            .map(res => res.json())
    }

    getProducttypes() {
        let body = JSON.stringify({});
        this.Url = API_URL+":2020/getProducttypes";
        return this.http.post(this.Url, body, this.options)
            .map(res => res.json());
    }
    getPrefsettingById(id) {
        let body = JSON.stringify({ _id: id });
        this.Url = API_URL+":2020/getPrefSettingById";
        return this.http.post(this.Url, body, this.options)
            .map(res => res.json());
    }

    incPriority(pref) {
        let body = JSON.stringify({ _id: pref._id, priority: pref.prirority, productType: pref.productType });
        this.Url = API_URL+":2020/incPriority";
        return this.http.post(this.Url, body, this.options)
            .map(res => res.json());
    }

    decPriority(pref) {
        let body = JSON.stringify({ _id: pref._id, priority: pref.prirority, productType: pref.productType });
        this.Url = API_URL+":2020/decPriority";
        return this.http.post(this.Url, body, this.options)
            .map(res => res.json());
    }
}
