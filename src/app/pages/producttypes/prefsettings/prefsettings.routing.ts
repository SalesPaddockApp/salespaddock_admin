import { Routes, RouterModule } from '@angular/router';

import { Prefsettings } from './prefsettings.component';
import { CreatePrefComponent } from './createpref/createpref.component'
import { EditPrefComponent } from './editpref/editpref.component';
const routes: Routes = [
  {
    path: '',
    component: Prefsettings,
    children: [
    ]
  },
  { path: 'create/:id', component: CreatePrefComponent },
  { path: 'edit/:id', component: EditPrefComponent }
];

export const routing = RouterModule.forChild(routes);
