import { Component, ViewEncapsulation, Input } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl, FormArray } from '@angular/forms';
import { PrefsettingsService } from '../prefsettings.service';

@Component({
    selector: 'editpref',
    encapsulation: ViewEncapsulation.None,
    template: require('./editpref.html'),
    styles: [require('./editpref.scss')]
})
export class EditPrefComponent {
    private prefsetting: any;
    private producttypes: any;
    private id: string;
    private editPrefForm: FormGroup;
    constructor(private route: ActivatedRoute,
        private router: Router, private prefsettingsService: PrefsettingsService, private fb: FormBuilder, private _location: Location) {
    }

    ngOnInit() {
        this.route.params.forEach((params: Params) => {
            this.id = params['id'];
        });
        this.prefsettingsService.getPrefsettingById(this.id)
            .subscribe((data) => {
                this.prefsetting = data.response.data;
                this.initEditPrefsettingForm()
                this.prefsetting.optionsValue.forEach((data) => {
                    const control = <FormArray>this.editPrefForm.controls['optionsValue']
                    control.push(this.createEditPrefOptions(data));
                })
                console.log(this.prefsetting)
            })
        this.prefsettingsService.getProducttypeById(this.id)
            .subscribe((data) => {
                this.producttypes = data.response.data.name;
            });
    }

    initEditPrefsettingForm() {
        this.editPrefForm = this.fb.group({
            "_id": this.prefsetting._id,
            "preferenceTitle": [this.prefsetting.preferenceTitle, Validators.compose([
                Validators.required])],
            "onCreateScreen": [this.prefsetting.onCreateScreen, Validators.compose([
                Validators.required])],
            "typeOfPreference": [{ value: this.prefsetting.typeOfPreference, disabled: true }, Validators.compose([
                Validators.required])],
            "productType": [{ value:this.prefsetting.productType, disabled: true }, Validators.compose([
                Validators.required])],
            "productId": [{ value:this.prefsetting.productId, disabled: true }, Validators.compose([
                Validators.required])],
            "mandatory": [this.prefsetting.mandatory, Validators.compose([
                Validators.required])],
            "Unit": [this.prefsetting.Unit, Validators.compose([
                Validators.required])],
            "optionsValue": this.fb.array([
            ])
        })
    }
    // initOptions() {
    //    return this.fb.control('', Validators.required) 
    // }
    createEditPrefOptions(val) {
        return this.fb.control(val, Validators.required)
    }
    addOptions() {
        var val = ''
        const control = <FormArray>this.editPrefForm.controls['optionsValue'];
        control.push(this.createEditPrefOptions(val));
    }

    deleteOptions(i: number) {
        const control = <FormArray>this.editPrefForm.controls['optionsValue'];
        control.removeAt(i)
    }
    editPref() {
        console.log(this.editPrefForm.value)
        this.prefsettingsService.editPrefsetting(this.editPrefForm.value)
            .subscribe((data) => {

                if (data.errCode == 0) {
                    this._location.back();
                }
                else {
                    alert(data.Message);
                }
            })

    }
    setMandatory(e) {
        if (e.target.checked) {
            console.log(this.editPrefForm.controls['mandatory'].value)
            this.editPrefForm.controls['mandatory'].setValue(1)
            console.log(this.editPrefForm.controls['mandatory'].value)
        }
        else {
            console.log(this.editPrefForm.controls['mandatory'].value)
            this.editPrefForm.controls['mandatory'].setValue(0);
            console.log(this.editPrefForm.controls['mandatory'].value)

        }
    }
    setOCS(e) {
        if (e.target.checked) {
            const control = this.editPrefForm.controls['onCreateScreen']
            // this.editPrefForm.controls['oneditScreen'].patchValue(1);
            control.patchValue(1)
        }
        else {
            const control = this.editPrefForm.controls['onCreateScreen']
            control.patchValue(0)
            // this.editPrefForm.controls['oneditScreen'].patchValue(0);
        }

    }
    goBack(): void {
        this._location.back();
    }
}
