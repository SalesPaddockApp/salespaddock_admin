import { Component, ViewEncapsulation, Input } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl, FormArray } from '@angular/forms';
import { PrefsettingsService } from '../prefsettings.service';

@Component({
    selector: 'createpref',
    encapsulation: ViewEncapsulation.None,
    template: require('./createpref.html'),
    styles: [require('./createpref.scss')]
})
export class CreatePrefComponent {
    private producttypes: any;
    private id: any;
    private msisdn: string;
    private TOP: any;
    private createPrefForm: FormGroup;
    constructor(private route: ActivatedRoute,
        private router: Router, private prefsettingsService: PrefsettingsService, private fb: FormBuilder, private _location: Location) {
    }

    ngOnInit() {
        this.route.params.forEach((params: Params) => {
            this.id = params['id'];
        });
        this.initAddPrefsettingForm()
        this.prefsettingsService.getProducttypeById(this.id)
            .subscribe((data) => {
                console.log(data)
                this.producttypes = data.response.data.name;
                this.createPrefForm.controls['productType'].setValue(this.producttypes)
            });
    }

    initAddPrefsettingForm() {
        this.createPrefForm = this.fb.group({
            "preferenceTitle": ['', Validators.compose([
                Validators.required])],
            "onCreateScreen": [0, Validators.compose([
                Validators.required])],
            "typeOfPreference": [this.TOP, Validators.compose([
                Validators.required])],
            "productType": [this.producttypes, Validators.compose([
                Validators.required])],
            "productId": [this.id, Validators.compose([
                Validators.required])],
            "mandatory": [0, Validators.compose([
                Validators.required])],
            "Unit": ['0', Validators.compose([
                Validators.required])],
            "optionsValue": this.fb.array([

            ]),
        })
    }
    initOptions() {
        // return this.fb.group({
        //     'title': ['', Validators.compose([
        //         Validators.required])]
        // })
        // let title = new FormControl('')
        // return title;
        return this.fb.control('', Validators.required)
    }

    addOptions() {
        const control = <FormArray>this.createPrefForm.controls['optionsValue'];
        control.push(this.initOptions());
    }

    deleteOptions(i: number) {
        const control = <FormArray>this.createPrefForm.controls['optionsValue'];
        control.removeAt(i)
    }
    createPref() {
        console.log(this.createPrefForm.value);

        this.prefsettingsService.addPrefsetting(this.createPrefForm.value)
            .subscribe((data) => {
                console.log(data)
                if (data.errCode == 0) {
                    this._location.back();
                }
                else {
                    alert(data.Message);
                }
            })

    }
    setMandatory(e) {
        if (e.target.checked) {
            this.createPrefForm.controls['mandatory'].setValue(1);
        }
        else {
            this.createPrefForm.controls['mandatory'].setValue(0);

        }
    }
    setOCS(e) {
        if (e.target.checked) {
            this.createPrefForm.controls['onCreateScreen'].setValue(1);
        }
        else {
            this.createPrefForm.controls['onCreateScreen'].setValue(0);
        }

    }
    check(e) {
        const control = <FormArray>this.createPrefForm.controls['optionsValue'];
        let length = control.length
        console.log(control.controls)
        console.log("control length " + length)
        while (length--) {
            control.removeAt(length)
            console.log(control.controls)
            console.log("index " + length)
        }
        console.log(control.controls)
        const val = e.target.value
        if (val == 1 || val == 2) {
            console.log(val)
            control.push(this.initOptions());
            console.log(control.controls)
        }
        else if (val == 4) {
            control.push(this.initOptions())
        }
        else if (val == 3) {
            control.push(this.initOptions())
            control.push(this.initOptions())
            console.log(control.controls)
        }
        else if (val == 8 || val == 5 || val == 6|| val == 7) {
            control.push(this.initOptions())
        }
        console.log(this.createPrefForm.value)
    }
    goBack(): void {
        this._location.back();
    }
}
