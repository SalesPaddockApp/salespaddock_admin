import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { BaThemeSpinner, BaThemePreloader } from '../../theme/services';
import { Location } from '@angular/common';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import { PrefsettingsService } from './prefsettings.service';
import { AuthService } from '../../../sharedServices/authService/auth.service';
import { OrderBy } from '../../sharedServices/pipes/orderByPipe.ts';

@Component({
  selector: 'prefsettings',
  encapsulation: ViewEncapsulation.None,
  template: require('./prefsettings.html'),
  styles: [require('./prefsettings.scss')]
})
export class Prefsettings implements OnInit {
  private addPrefsettingForm: FormGroup;
  private editPrefsettingForm: FormGroup;
  private prefsettingById: any;
  prefsettings: any;
  private id: any;
  private nodata: boolean = false;
  private defaultprefsettings: any;
  private productName: any;
  searchControl = new FormControl();
  private ids: any[] = [];
  constructor(private route: ActivatedRoute, private _location: Location, private prefsettingsService: PrefsettingsService, private fb: FormBuilder,
    private authService: AuthService, private router: Router) {
  }

  ngOnInit() {
    this.authService.checkCredentials();
    this.route.params.forEach((params: Params) => {
      this.id = params['id'];
      console.log('yunus this.id '+ this.id )
    });
    this.prefsettingsService.getProducttypeById(this.id)
      .subscribe((data) => {
        console.log(data)
        this.productName = data.response.data.name
      })
    this.getprefsettings();
    this.searchControl.valueChanges
      .debounceTime(400)
      .distinctUntilChanged()
      .flatMap(seachControl => this.prefsettingsService.search(seachControl))
      .subscribe(data => {
        if (data.errCode == 0) {
          this.prefsettings = data.response.data
        }
        else {
          this.prefsettingsService.getPrefsettings(this.id)
            .subscribe((res) => {
              this.prefsettings = res.response.data;
            });
        }
      });
    this.initAddPrefsettingForm()
    this.prefsettingsService.getPrefToCreatePost(this.id,"noidexists")
      .subscribe((res) => {
        if (res.errCode == 0) {
          this.defaultprefsettings = res.response.data[0];
        }
        else {
          this.nodata = true
        }
      });
  }
  initAddPrefsettingForm() {
    this.addPrefsettingForm = this.fb.group({
      "productName": ['', Validators.compose([
        Validators.required])]
    })
  }
  getPrefsettingById(prefsetting) {
    this.prefsettingById = prefsetting;
    this.editPrefsettingForm = this.fb.group({
      "_id": this.prefsettingById._id,
      "productName": this.prefsettingById.productName
    })
  }

  getprefsettings() {
    this.prefsettingsService.getPrefsettings(this.id)
      .subscribe((res) => {
        console.log(res);
        if (res.errCode == 0) {
          this.nodata = false
          this.prefsettings = res.response.data;
        }
        else {
          this.nodata = true
        }
      });
  }

  addPrefsetting() {
    this.prefsettingsService.addPrefsetting(this.addPrefsettingForm.value)
      .subscribe((data) => {
        if (data.errCode == 0) {
          this.initAddPrefsettingForm()
          this.prefsettingsService.getPrefsettings(this.id)
            .subscribe(data => {
              this.nodata = false
              this.prefsettings = data.response.data;
            });
        }
      })
  }

  editPrefsetting() {
    this.router.navigate(['edit', this.ids[0]], { relativeTo: this.route });
  }

  deletePrefsetting(PrefsettingId, prirority, productType) {
    console.log(PrefsettingId)
    this.prefsettingsService.deletePrefsetting(PrefsettingId, prirority, productType)
      .subscribe((data) => {
        console.log(data)
        if (data.errCode == 0) {
          this.prefsettings = null
          this.prefsettingsService.getPrefsettings(this.id)
            .subscribe(data => {
              if (data.errCode == 0) {
                this.nodata = false
                this.prefsettings = data.response.data;
              }
              else {
                this.nodata = true
              }
            });
        }
      })
  }

  incPriority(pref) {
    console.log(pref)
    this.prefsettingsService.incPriority(pref)
      .subscribe((data) => {
        console.log(data)
        if (data.errCode == 0) {
          this.prefsettingsService.getPrefsettings(this.id)
            .subscribe(data => {
              this.prefsettings = data.response.data;
            });
        }
      })
  }

  decPriority(pref) {
    console.log(pref)
    this.prefsettingsService.decPriority(pref)
      .subscribe((data) => {
        console.log(data)
        if (data.errCode == 0) {
          this.prefsettingsService.getPrefsettings(this.id)
            .subscribe(data => {
              this.prefsettings = data.response.data;
            });
        }
      })
  }
  goBack(): void {
    this._location.back();
  }

  create() {
    this.router.navigate(['create', this.id], { relativeTo: this.route });
  }
  setIds(e, producttype) {
    if (e.target.checked) {
      this.ids.push(producttype._id)
      this.prefsettingById = producttype
    }
    else {
      this.ids.splice(this.ids.indexOf(producttype._id));
    }
    console.log(this.ids)
  }
}
