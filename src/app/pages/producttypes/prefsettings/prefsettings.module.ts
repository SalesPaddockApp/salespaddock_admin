import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgaModule } from '../../../theme/nga.module';
import { ReactiveFormsModule } from '@angular/forms';
import { routing }       from './prefsettings.routing';
import { Prefsettings } from './prefsettings.component';
import { PrefsettingsService } from './prefsettings.service';
import { CreatePrefComponent } from './createpref/createpref.component';
import { EditPrefComponent } from './editpref/editpref.component';
import { PagesModule } from '../../pages.module'

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgaModule,
    routing,
    ReactiveFormsModule,
    PagesModule
  ],
  declarations: [
    Prefsettings,
    CreatePrefComponent,
    EditPrefComponent
  ],
  providers: [
    PrefsettingsService
  ]
})
export default class prefsettingsModule {}
