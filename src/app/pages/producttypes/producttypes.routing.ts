import { Routes, RouterModule } from '@angular/router';

import { Producttypes } from './producttypes.component';

// noinspection TypeScriptValidateTypes
const routes: Routes = [
  {
    path: '',
    component: Producttypes,
    children: [
    ]
  },
  { path: 'prefsettings/:id', loadChildren: () => System.import('./prefsettings/prefsettings.module') }
];

export const routing = RouterModule.forChild(routes);
