import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class ProducttypesService {
    private Url: string;
    private headers = new Headers({
        'Content-Type':'application/json',
        "authorization": 'Basic ' + btoa("horse" + ":" + "123456"),
        "x-access-token": localStorage.getItem("user")
    })
    private options = new RequestOptions({
        headers: this.headers
    })
    constructor(private http: Http) {
        
    }
    getProducttypes() {
        let body = JSON.stringify({});
        let options = new RequestOptions({ headers: this.headers });
        this.Url = API_URL+":2020/getProducttypes";
        return this.http.post(this.Url, body, options)
            .map(res => res.json());
    }
    addProducttype(producttypeToAdd) {
        // let options = new RequestOptions({ headers: this.headers });
        let body = JSON.stringify(producttypeToAdd);
        this.Url = API_URL+":2020/createProducttype";
        return this.http.post(this.Url, body, this.options)
            .map(res => res.json());
    }

    deleteProducttype(producttypeId) {
        let options = new RequestOptions({ headers: this.headers });
        let body = JSON.stringify({ _ids: producttypeId });
        let Url = API_URL+":2020/deleteProducttype";
        return this.http.post(Url, body, options)
            .map(res => res.json());
    }
    editProducttype(producttype) {
        let options = new RequestOptions({ headers: this.headers });
        let body = JSON.stringify(producttype);
        this.Url = API_URL+":2020/editProducttype";
        return this.http.post(this.Url, body, options)
            .map(res => res.json())

    }
    search(seachControl){
        let options = new RequestOptions({ headers: this.headers });
        let body = JSON.stringify({productName: seachControl});
        this.Url = API_URL+":2020/searchProducttypes";
        return this.http.post(this.Url, body, options)
            .map(res => res.json())
    }
}
