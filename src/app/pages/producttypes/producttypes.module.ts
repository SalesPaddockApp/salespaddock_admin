import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgaModule } from '../../theme/nga.module';
import { ReactiveFormsModule } from '@angular/forms';
import { routing }       from './producttypes.routing';
import { Producttypes } from './producttypes.component';
import { ProducttypesService } from './producttypes.service';
import { PagesModule } from '../pages.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgaModule,
    routing,
    ReactiveFormsModule,
    PagesModule
  ],
  declarations: [
    Producttypes
  ],
  providers: [
    ProducttypesService
  ]
})
export default class producttypesModule {}
