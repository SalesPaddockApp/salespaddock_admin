import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { BaThemeSpinner, BaThemePreloader } from '../../theme/services';
import { Location } from '@angular/common';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import { ProducttypesService } from './producttypes.service';
import { AuthService } from '../../sharedServices/authService/auth.service';
import { FileUploader } from 'ng2-file-upload';
import { ShowsService } from '../shows.service';
const URL = API_URL+':8009/upload';

@Component({
  selector: 'producttypes',
  encapsulation: ViewEncapsulation.None,
  template: require('./producttypes.html'),
  styles: [require('./producttypes.scss')]
})
export class Producttypes implements OnInit {
  private Imgage: any;
  private imageurl: any;
  private editimageurl: any;
  private addProducttypeForm: FormGroup;
  private editProducttypeForm: FormGroup;
  private producttypeById: any;
  producttypes: any;
  searchControl = new FormControl();
  private ids: any[] = [];
  private producttype: any;

  constructor(private _location: Location, private producttypesService: ProducttypesService, private fb: FormBuilder,
    private authService: AuthService, private router: Router) {
  }

  ngOnInit() {
    let _self = this;
    this.authService.checkCredentials();
    this.getProducttypes();
    this.searchControl.valueChanges
      .debounceTime(400)
      .distinctUntilChanged()
      .flatMap(seachControl => this.producttypesService.search(seachControl))
      .subscribe(data => {
        if (data.errCode == 0) {
          console.log(data)
          this.producttypes = data.response.data
        }
        else {
          this.producttypesService.getProducttypes()
            .subscribe((res) => {
              this.producttypes = res.response.data;
            });
        }
      });
    this.initAddProducttypeForm()

    this.uploader.onSuccessItem = function (item: any, response: any, status: number, headers: any) {
      _self.Imgage = JSON.parse(response);
      _self.imageurl = (API_URL+":8009/" + _self.Imgage.url)
      console.log(_self.imageurl);
      if (_self.addProducttypeForm) {
        if (_self.editProducttypeForm) {
          _self.editProducttypeForm.controls['imgPath'].setValue(_self.imageurl)
        }
        _self.addProducttypeForm.controls['imgPath'].setValue(_self.imageurl)
      }
      else if (_self.editProducttypeForm) {
        _self.editProducttypeForm.controls['imgPath'].setValue(_self.imageurl)
        if (_self.addProducttypeForm) {
          _self.addProducttypeForm.controls['imgPath'].setValue(_self.imageurl)
        }
      }
      return { item, response, status, headers };
    };

  }

  public uploader: FileUploader = new FileUploader({ url: URL, autoUpload: true, itemAlias: "photo" });
  public hasBaseDropZoneOver: boolean = false;
  public hasAnotherDropZoneOver: boolean = false;

  public fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }

  public fileOverAnother(e: any): void {
    this.hasAnotherDropZoneOver = e;
  }

  initAddProducttypeForm() {
    this.addProducttypeForm = this.fb.group({
      "productName": ['', Validators.compose([
        Validators.required])],
      "imgPath": ['', Validators.compose([
        Validators.required])]
    })
  }
  getProducttypeById() {
    this.producttypeById = this.producttype;
    this.imageurl = this.producttypeById.imgPath;
    this.editProducttypeForm = this.fb.group({
      "_id": this.producttypeById._id,
      "productName": this.producttypeById.productName,
      "imgPath": this.producttypeById.imgPath
    })
  }

  getProducttypes() {
    BaThemePreloader.load().then((values) => {
      this.producttypesService.getProducttypes()
        .subscribe((res) => {
          if (res.errCode == 0) {
            console.log(res)
            this.producttypes = res.response.data;
          }
          else {

          }
        });
    });
  }

  addProducttype() {
    this.producttypesService.addProducttype(this.addProducttypeForm.value)
      .subscribe((data) => {
        if (data.errCode == 0) {
          this.ids = []
          this.initAddProducttypeForm()
          this.imageurl = null;
          this.producttypesService.getProducttypes()
            .subscribe(data => {
              this.producttypes = data.response.data;
            });
        }
      })
  }

  editProducttype() {
    console.log(this.editProducttypeForm.value)
    this.producttypesService.editProducttype(this.editProducttypeForm.value)
      .subscribe((data) => {
        if (data.errCode == 0) {
          this.ids = []
          this.initAddProducttypeForm()
          this.imageurl = null
          this.producttypesService.getProducttypes()
            .subscribe(data => {
              this.producttypes = data.response.data;
            });
        }
      })
  }

  deleteProducttype() {
    this.producttypesService.deleteProducttype(this.ids)
      .subscribe((data) => {
        console.log(data)
        if (data.errCode == 0) {
          this.ids = []
          this.producttypesService.getProducttypes()
            .subscribe(data => {
              this.producttypes = data.response.data;
            });
        }
      })
  }
  getPreference(id) {
    this.router.navigate(['pages/producttypes/prefsettings', id]);
  }
  setIds(e, producttype){
    if(e.target.checked){
      this.ids.push(producttype._id)
      this.producttype = producttype
    }
    else{
      this.ids.splice(this.ids.indexOf(producttype._id));
    }
    console.log(this.ids)
  }
}
