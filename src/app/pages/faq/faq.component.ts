import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { BaThemeSpinner, BaThemePreloader } from '../theme/services';
import { Location } from '@angular/common';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import { FAQService } from './faq.service';
import { AuthService } from '../../sharedServices/authService/auth.service';
import { FileUploader } from 'ng2-file-upload';
const URL = API_URL+':8009/upload';

@Component({
  selector: 'faq',
  encapsulation: ViewEncapsulation.None,
  template: require('./faq.html'),
  styles: [require('./faq.scss')]
})
export class FAQ implements OnInit {
  private Imgage: any;
  private imageurl: any;
  private editimageurl: any;
  private addfaqForm: FormGroup;
  private editfaqForm: FormGroup;
  private faqById: any;
  faq: any;
  private nodata: boolean = false
  searchControl = new FormControl();
  private device: any;
  private ids: any[] = [];
  private f: any;
  private question: any = '';
  private images: any = [
    {
      img: "http://128.199.54.219:8009/stickers/10.png"
    },
    {
      img: "http://128.199.54.219:8009/stickers/10.png"
    },
    {
      img: "http://128.199.54.219:8009/stickers/10.png"
    },
    {
      img: "http://128.199.54.219:8009/stickers/10.png"
    }
  ];

  constructor(private _location: Location, private faqService: FAQService, private fb: FormBuilder,
    private authService: AuthService, private router: Router) {
  }

  ngOnInit() {
    let _self = this;
    this.authService.checkCredentials();
    this.getFAQ();

    this.searchControl.valueChanges
      .debounceTime(400)
      .distinctUntilChanged()
      .flatMap(seachControl => this.faqService.search(seachControl))
      .subscribe(data => {
        if (data.errCode == 0) {
          this.faq = data.response.data
        }
        else {
          this.faqService.getFAQ()
            .subscribe((res) => {
              if (res.errCode == 0) {
                this.nodata = false
                this.faq = res.response.data;
              }
              else {
                this.nodata = true
              }
            });
        }
      });
    this.uploader.onSuccessItem = function (item: any, response: any, status: number, headers: any) {
      _self.Imgage = JSON.parse(response);
      _self.imageurl = (API_URL+":8009/" + _self.Imgage.url)
      console.log(_self.imageurl);
      if (_self.addfaqForm) {
        if (_self.editfaqForm) {
          _self.editfaqForm.controls['img'].setValue(_self.imageurl)
        }
        _self.addfaqForm.controls['img'].setValue(_self.imageurl)
      }
      else if (_self.editfaqForm) {
        _self.editfaqForm.controls['img'].setValue(_self.imageurl)
        if (_self.addfaqForm) {
          _self.addfaqForm.controls['img'].setValue(_self.imageurl)
        }
      }
      return { item, response, status, headers };
    };
    this.initAddfaqForm()
  }

  public uploader: FileUploader = new FileUploader({ url: URL, autoUpload: true, itemAlias: "photo" });
  public hasBaseDropZoneOver: boolean = false;
  public hasAnotherDropZoneOver: boolean = false;

  public fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }

  public fileOverAnother(e: any): void {
    this.hasAnotherDropZoneOver = e;
  }


  initAddfaqForm() {
    this.addfaqForm = this.fb.group({
      "question": ['', Validators.compose([
        Validators.required])],
      "answer": ['', Validators.compose([
        Validators.required])]
    })
  }
  getfaqById() {
    this.faqById = this.f;
    this.editfaqForm = this.fb.group({
      "_id": this.faqById._id,
      "question": [this.faqById.question, Validators.compose([Validators.required])],
      "answer": [this.faqById.answer, Validators.compose([Validators.required])]
    })
  }

  getFAQ() {
    this.faqService.getFAQ()
      .subscribe((res) => {
        console.log(res)
        if (res.errCode == 0) {
          this.nodata = false
          this.faq = res.response.data;
        }
        else {
          this.nodata = true
        }
      });
  }
  editFAQ() {
    this.router.navigate(['/pages/faq/edit', this.ids[0]]);
  }
  deleteFAQ() {
    this.faqService.deleteFAQ(this.ids)
      .subscribe((data) => {
        this.ids = []
        if (data.errCode == 0) {
          this.faq = null;
          this.faqService.getFAQ()
            .subscribe(data => {
              if (data.errCode == 0) {
                this.nodata = false
                this.faq = data.response.data;
              }
              else {
                this.nodata = true
              }
            });
        }
      })
  }

  setIds(e, producttype) {
    if (e.target.checked) {
      this.ids.push(producttype._id)
      this.f = producttype
      this.getfaqById();
    }
    else {
      this.ids.splice(this.ids.indexOf(producttype._id));
    }
    console.log(this.ids)
  }
  addImage(img) {
    this.question = this.question + "<img src=" + img + ">"
    console.log(this.question)
  }
  createFAQ() {
    this.router.navigate(['/pages/faq/create']);
  }
}
