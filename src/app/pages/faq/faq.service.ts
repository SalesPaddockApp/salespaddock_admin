import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class FAQService {
    private Url: string;
    private headers = new Headers({
        'Content-Type': 'application/json',
        "authorization": 'Basic ' + btoa("horse" + ":" + "123456"),
        "x-access-token": localStorage.getItem("user")
    })
    private options = new RequestOptions({
        headers: this.headers
    })
    constructor(private http: Http) {

    }
    getFAQ() {
        let body = JSON.stringify({});
        let options = new RequestOptions({ headers: this.headers });
        this.Url = API_URL+":2020/getFAQ";
        return this.http.post(this.Url, body, options)
            .map(res => res.json());
    }
    addFAQ(question, answer) {
        // let options = new RequestOptions({ headers: this.headers });
        let body = JSON.stringify({ question: question, answer: answer });
        this.Url = API_URL+":2020/createFAQ";
        return this.http.post(this.Url, body, this.options)
            .map(res => res.json());
    }

    deleteFAQ(FAQId) {
        let options = new RequestOptions({ headers: this.headers });
        let body = JSON.stringify({ _ids: FAQId });
        let Url = API_URL+":2020/deleteFAQ";
        return this.http.post(Url, body, options)
            .map(res => res.json());
    }
    editFAQ(question, answer, id) {
        let options = new RequestOptions({ headers: this.headers });
        let body = JSON.stringify({ question: question, answer: answer, _id: id });
        this.Url = API_URL+":2020/editFAQ";
        return this.http.post(this.Url, body, options)
            .map(res => res.json())

    }
    search(seachControl) {
        let options = new RequestOptions({ headers: this.headers });
        let body = JSON.stringify({ searchP: seachControl });
        this.Url = API_URL+":2020/searchFAQ";
        return this.http.post(this.Url, body, options)
            .map(res => res.json())
    }
    getFAQBy_Id(id) {
        let body = JSON.stringify({ _id: id });
        this.Url = API_URL+":2020/getFAQBy_Id";
        return this.http.post(this.Url, body, this.options)
            .map(res => res.json());
    }
    makeActive(id) {
        let body = JSON.stringify({ _id: id });
        this.Url = API_URL+":2020/makeFAQActive";
        return this.http.post(this.Url, body, this.options)
            .map(res => res.json());
    }
    makeInactive(id) {
        let body = JSON.stringify({ _id: id });
        this.Url = API_URL+":2020/makeFAQInactive";
        return this.http.post(this.Url, body, this.options)
            .map(res => res.json());
    }
    getDeviceLog(id) {
        let body = JSON.stringify({ _id: id });
        this.Url = API_URL+":2020/getDeviceLog";
        return this.http.post(this.Url, body, this.options)
            .map(res => res.json());
    }
    getAllImages() {
        let body = JSON.stringify({});
        this.Url = API_URL+":2020/getAllImages";
        return this.http.post(this.Url, body, this.options)
            .map(res => res.json());
    }
    getFAQById(id) {
        let body = JSON.stringify({ _id: id });
        this.Url = API_URL+":2020/getFAQById";
        return this.http.post(this.Url, body, this.options)
            .map(res => res.json());
    }
}
