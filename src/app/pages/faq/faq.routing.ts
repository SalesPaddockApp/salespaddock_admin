import { Routes, RouterModule } from '@angular/router';
import { Create } from './create/create.component';
import { Edit } from './edit/edit.component';

import { FAQ } from './faq.component';
const routes: Routes = [
  {
    path: '',
    component: FAQ,
    children: [
    ]
  },
  { path: 'create', component: Create },
  { path: 'edit/:id', component: Edit }
];

export const routing = RouterModule.forChild(routes);
