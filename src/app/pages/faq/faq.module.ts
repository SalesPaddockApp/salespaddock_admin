import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgaModule } from '../../theme/nga.module';
import { ReactiveFormsModule } from '@angular/forms';
import { routing }       from './faq.routing';
import { FAQ } from './faq.component';
import { FAQService } from './faq.service';
import { PagesModule } from '../pages.module';
import { PostsService } from '../posts/posts.service';
import { Create } from './create/create.component';
import { Edit } from './edit/edit.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgaModule,
    routing,
    ReactiveFormsModule,
    PagesModule
  ],
  declarations: [
    FAQ,
    Create,
    Edit
  ],
  providers: [
    FAQService,
    PostsService
  ]
})
export default class FAQModule {}
