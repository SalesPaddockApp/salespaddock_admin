import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { BaThemeSpinner, BaThemePreloader } from '../theme/services';
import { Location } from '@angular/common';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import { FAQService } from '../faq.service';
import { AuthService } from '../../../sharedServices/authService/auth.service';
import { FileUploader } from 'ng2-file-upload';
const URL = API_URL+':8009/imageUpload';
import { ImageCropperComponent, CropperSettings } from 'ng2-img-cropper';

@Component({
    selector: 'edit',
    encapsulation: ViewEncapsulation.None,
    template: require('./edit.html')
})
export class Edit implements OnInit {
    private Imgage: any;
    private imageurl: any;
    private editimageurl: any;
    private addfaqForm: FormGroup;
    private editfaqForm: FormGroup;
    private faqById: any;
    faq: any;
    private nodata: boolean = false
    searchControl = new FormControl();
    private device: any;
    private ids: any[] = [];
    private f: any;
    private question: any = '';
    private answer: any = '';
    private images: any = [];
    private id: any;
    private data: any;
    private uploaded: boolean = false;

    filesToUpload: Array<File>;

    cropperSettings: CropperSettings;

    constructor(private _location: Location, private faqService: FAQService, private fb: FormBuilder,
        private authService: AuthService, private router: Router, private route: ActivatedRoute) {

        this.filesToUpload = [];
        this.cropperSettings = new CropperSettings();
        this.cropperSettings.width = 320;
        this.cropperSettings.height = 240;
        this.cropperSettings.croppedWidth = 320;
        this.cropperSettings.croppedHeight = 240;
        this.cropperSettings.canvasWidth = 500;
        this.cropperSettings.canvasHeight = 500;
        this.data = {};
    }

    ngOnInit() {
        this.route.params.forEach((params: Params) => {
            this.id = params['id'];
        });
        this.faqService.getFAQById(this.id)
            .subscribe((data) => {
                console.log(data)
                if (data.errCode == 0) {
                    this.question = data.response.data.question;
                    this.answer = data.response.data.answer;
                }
            })
        let _self = this;
        this.authService.checkCredentials();
        this.faqService.getAllImages()
            .subscribe((data) => {
                if (data.errNum == 0) {
                    this.images = data.stickers
                }
            })
        this.uploader.onSuccessItem = function (item: any, response: any, status: number, headers: any) {
            _self.Imgage = JSON.parse(response);
            _self.imageurl = (API_URL+":8009/" + _self.Imgage.url)
            console.log(_self.imageurl);
            _self.images.push(_self.imageurl)
            return { item, response, status, headers };
        };
    }

    public uploader: FileUploader = new FileUploader({ url: URL, autoUpload: true, itemAlias: "photo" });
    public hasBaseDropZoneOver: boolean = false;
    public hasAnotherDropZoneOver: boolean = false;

    public fileOverBase(e: any): void {
        this.hasBaseDropZoneOver = e;
    }

    public fileOverAnother(e: any): void {
        this.hasAnotherDropZoneOver = e;
    }


    editFAQ() {
        this.faqService.editFAQ(this.question, this.answer, this.id)
            .subscribe((data) => {
                console.log(data)
                if (data.errCode == 0) {
                    this._location.back()
                }
            })
    }
    addImage(img) {
        this.answer = this.answer + "<img src=" + img + ">"
        console.log(this.answer)
    }
    goBack() {
        this._location.back()
    }
    upload(i, j) {
        this.makeFileRequest(URL, [], this.filesToUpload[0]).then((result) => {
            console.log(JSON.stringify(result['url']));
            const url = result['url'];
            this.images.push(API_URL+":8009/" + url)
            console.log(this.imageurl)
            this.uploaded = true;
        },
            (error) => {
                console.error(error);
            });
    }

    fileChangeEvent(fileInput: any, i, j) {
        this.filesToUpload = <Array<File>>fileInput.target.files;
        this.upload(i, j);
        const name = this.filesToUpload[0]
        console.log(this.filesToUpload[0])
    }

    makeFileRequest(url: string, params: Array<string>, files: any) {
        return new Promise((resolve, reject) => {
            var formData: any = new FormData();
            var xhr = new XMLHttpRequest();
            formData.append("photo", files, files.name)
            console.log(formData)
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4) {
                    if (xhr.status == 200) {
                        resolve(JSON.parse(xhr.response));
                    } else {
                        reject(xhr.response);
                    }
                }
            }
            xhr.open("POST", url, true);
            xhr.send(formData);
        });
    }

    confirmUpload(i) {
        console.log(this.data.image)
        let dataURI = this.data.image;
        function dataURItoBlob(dataURI: string) {
            // convert base64 to raw binary data held in a string
            // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
            var byteString = atob(dataURI.split(',')[1]);

            // separate out the mime component
            var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]

            // write the bytes of the string to an ArrayBuffer
            var ab = new ArrayBuffer(byteString.length);
            var ia = new Uint8Array(ab);
            for (var i = 0; i < byteString.length; i++) {
                ia[i] = byteString.charCodeAt(i);
            }

            // write the ArrayBuffer to a blob, and you're done
            var blob = new Blob([ab], { type: mimeString });
            return blob;

            // Old code
            // var bb = new BlobBuilder();
            // bb.append(ab);
            // return bb.getBlob(mimeString);
        }
        var blob = dataURItoBlob(dataURI);
        var fileName = (Math.random().toString(36).substring(7) + ".png");
        let imgagefile = new File([blob], fileName)
        console.log(blob)
        this.makeFileRequest(URL, [], imgagefile).then((result) => {
            console.log(JSON.stringify(result['url']));
            const url = result['url'];
            this.images.push(API_URL+":8009/" + url)
            this.uploaded = true
        })
    }
}
