import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { BaThemeSpinner, BaThemePreloader } from '../../../theme/services';
import { Location } from '@angular/common';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import { UsersService } from '../users.service';
import { AuthService } from '../../../sharedServices/authService/auth.service';

@Component({
  selector: 'inactive',
  encapsulation: ViewEncapsulation.None,
  template: require('./inactive.html')
})
export class InActive implements OnInit {
  private addUserForm: FormGroup;
  private editUserForm: FormGroup;
  private userById: any;
  users: any;
  private nodata: boolean = false
  searchControl = new FormControl();

  private moreData: any[];
  private pageNum: number = 0;
  private disable = false;
  private nouserresult: any;
  private ids: any[] = [];
  private user: any;

  constructor(private _location: Location, private usersService: UsersService, private fb: FormBuilder,
    private authService: AuthService, private router: Router) {
  }

  ngOnInit() {
    this.authService.checkCredentials();
    this.getInactiveUsers();

    this.searchControl.valueChanges
      .debounceTime(400)
      .distinctUntilChanged()
      .flatMap(seachControl => this.usersService.search(seachControl))
      .subscribe(data => {
        if (data.errCode == 0) {
          this.users = data.response.data
          this.disable = true
        }
        else if (data.errNum == 132) {
          this.pageNum = 0;
          this.usersService.getInactiveUsers(this.pageNum)
            .subscribe((res) => {
              if (res.errCode == 0) {
                this.nodata = false
                this.disable = false
                this.users = res.response.data;
                if (this.users.length % 10 != 0)
                  this.disable = true;
              }
              else {
                this.nodata = true
              }
            });
        }
        else {
          this.users = null;
          this.nouserresult = true
        }
      });
    this.initAddUserForm()
  }
  initAddUserForm() {
    this.addUserForm = this.fb.group({
      "productName": ['', Validators.compose([
        Validators.required])]
    })
  }
  getUserById(User) {
    this.userById = User;
    this.editUserForm = this.fb.group({
      "_id": this.userById._id,
      "userId": [{ value: this.userById.userId, disabled: true }, Validators.compose([Validators.required])],
      "dob": [this.userById.dob, Validators.compose([Validators.required])],
      "email": this.fb.group({
        "primaryEmail": [this.userById.email.primaryEmail, Validators.compose([Validators.required])]
      }),
      "name": this.fb.group({
        "fName": [this.userById.name.fName, Validators.compose([Validators.required])],
        "lName": [this.userById.name.lName, Validators.compose([])]
      })
    })
  }

  getInactiveUsers() {
    BaThemePreloader.load().then((values) => {
      this.usersService.getInactiveUsers(this.pageNum)
        .subscribe((res) => {
          if (res.errCode == 0) {
            this.nodata = false
            this.users = res.response.data;
            if (this.users.length % 10 != 0)
              this.disable = true;
          }
          else {
            this.nodata = true
          }
        });
    });
  }

  editUser() {
    this.router.navigate(['/pages/users/edit', this.ids[0]]);
  }

  deleteUser() {
    this.usersService.deleteUser(this.user.userId)
      .subscribe((data) => {
        this.ids = []
        if (data.errCode == 0) {
          this.usersService.getInactiveUsers(this.pageNum)
            .subscribe(data => {
              if (data.errCode == 0) {
                this.nodata = false
                this.users = data.response.data;
              }
              else {
                this.nodata = true
              }
            });
        }
      })
  }
  makeUserActive() {
    this.usersService.makeActive(this.ids)
      .subscribe((data) => {
        if (data.errCode == 0) {
          this.usersService.getInactiveUsers(this.pageNum)
            .subscribe(data => {
              if (data.errCode == 0) {
                this.nodata = false
                this.users = data.response.data;
              }
              else {
                this.users = null
                this.nodata = true
              }
            });
        }
      })
  }
  getPosts(id) {
    this.router.navigate(['/pages/users/posts', id]);
  }
  loadMore() {
    this.pageNum = this.pageNum + 1;
    this.usersService.getInactiveUsers(this.pageNum)
      .subscribe(res => {
        console.log(res)
        this.moreData = res.response.data;
        if (res.errCode == 0) {
          let len = this.moreData.length;
          for (let i = 0; i < len; i++)
            this.users.push(this.moreData[i]);
        }
        if (this.users.length % 10 != 0)
          this.disable = true;
      });
  }

  setIds(e, producttype) {
    if (e.target.checked) {
      this.ids.push(producttype._id)
      this.user = producttype
    }
    else {
      this.ids.splice(this.ids.indexOf(producttype._id));
    }
    console.log(this.ids)
  }
  getDeviceLog(id) {
    this.router.navigate(['/pages/users/log', id]);
    // this.usersService.getDeviceLog(id)
    //   .subscribe((res) => {
    //     if (res.errCode == 0) {
    //       console.log(res)
    //       this.device = res.response.data;
    //     }
    //     else {
    //       this.nodata = true
    //     }
    //   });
  }
}
