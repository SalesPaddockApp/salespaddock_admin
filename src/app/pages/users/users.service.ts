import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class UsersService {
    private Url: string;
    private headers = new Headers({
        'Content-Type': 'application/json',
        "authorization": 'Basic ' + btoa("horse" + ":" + "123456"),
        "x-access-token": localStorage.getItem("user")
    })
    private options = new RequestOptions({
        headers: this.headers
    })
    constructor(private http: Http) {

    }
    getActiveUsers(n) {
        let body = JSON.stringify({ pageNum: n });
        let options = new RequestOptions({ headers: this.headers });
        this.Url = API_URL+":2020/getActiveUsers";
        return this.http.post(this.Url, body, options)
            .map(res => res.json());
    }
    getInactiveUsers(n) {
        let body = JSON.stringify({ pageNum: n });
        let options = new RequestOptions({ headers: this.headers });
        this.Url = API_URL+":2020/getInactiveUsers";
        return this.http.post(this.Url, body, options)
            .map(res => res.json());
    }
    addUser(UserToAdd) {
        // let options = new RequestOptions({ headers: this.headers });
        let body = JSON.stringify(UserToAdd);
        this.Url = API_URL+":2020/createUser";
        return this.http.post(this.Url, body, this.options)
            .map(res => res.json());
    }

    deleteUser(UserId) {
        let options = new RequestOptions({ headers: this.headers });
        let body = JSON.stringify({ userId: UserId });
        let Url = API_URL+":2020/deleteUser";
        return this.http.post(Url, body, options)
            .map(res => res.json());
    }
    editUser(User) {
        let options = new RequestOptions({ headers: this.headers });
        let body = JSON.stringify(User);
        this.Url = API_URL+":2020/editUser";
        return this.http.post(this.Url, body, options)
            .map(res => res.json())

    }
    search(seachControl) {
        let options = new RequestOptions({ headers: this.headers });
        let body = JSON.stringify({ searchP: seachControl });
        this.Url = API_URL+":2020/searchUser";
        return this.http.post(this.Url, body, options)
            .map(res => res.json())
    }
    getUserBy_Id(id) {
        let body = JSON.stringify({ _id: id });
        this.Url = API_URL+":2020/getUserBy_Id";
        return this.http.post(this.Url, body, this.options)
            .map(res => res.json());
    }
    makeActive(id) {
        let body = JSON.stringify({ _ids: id });
        this.Url = API_URL+":2020/makeUserActive";
        return this.http.post(this.Url, body, this.options)
            .map(res => res.json());
    }
    makeInactive(id) {
        let body = JSON.stringify({ _ids: id });
        this.Url = API_URL+":2020/makeUserInactive";
        return this.http.post(this.Url, body, this.options)
            .map(res => res.json());
    }
    getDeviceLog(id) {
        let body = JSON.stringify({ userId: id });
        this.Url = API_URL+":2020/getDeviceLog";
        return this.http.post(this.Url, body, this.options)
            .map(res => res.json());
    }
    sendNotification(message, userIds) {

        // alert(1);

        let body = JSON.stringify({ mess: message.message, userId: userIds });
        this.Url = API_URL+":5005/notifyAllUsers";
        return this.http.post(this.Url, body, this.options)
            .map(res => res);
    }
}
