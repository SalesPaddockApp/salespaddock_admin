import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { BaThemeSpinner, BaThemePreloader } from '../../theme/services';
import { Location } from '@angular/common';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import { PostsService } from '../../posts/posts.service';
import { AuthService } from '../../../sharedServices/authService/auth.service';

@Component({
  selector: 'posts',
  encapsulation: ViewEncapsulation.None,
  template: require('./posts.html')
})
export class Posts implements OnInit {
  private addPostForm: FormGroup;
  private editpostForm: FormGroup;
  private postById: any;
  private postByIdToView: any;
  private id: string;
  posts: any;
  private nodata: boolean = false;
  private user: any;
  searchControl = new FormControl();
  constructor(private route: ActivatedRoute, private _location: Location, private postsService: PostsService, private fb: FormBuilder,
    private authService: AuthService, private router: Router) {
  }

  ngOnInit() {
    this.authService.checkCredentials();
    this.route.params.forEach((params: Params) => {
      this.id = params['id'];
    });
    this.getAllPosts();
    // this.searchControl.valueChanges
    //   .debounceTime(400)
    //   .distinctUntilChanged()
    //   .flatMap(seachControl => this.postsService.searchUser(seachControl))
    //   .subscribe(data => {
    //     if (data.errCode == 0) {
    //       this.posts = data.response.data
    //     }
    //     else {
    //       alert(data.Message)
    //       this.postsService.getAllActivePosts()
    //         .subscribe((res) => {
    //           this.posts = res.response.data;
    //         });
    //     }
    //   });
  }
  getpostById(post) {
    this.postById = post;
    this.editpostForm = this.fb.group({
      "_id": this.postById._id,
      "productName": this.postById.productName
    })
  }

  getAllPosts() {
    this.postsService.getPostByUser(this.id)
      .subscribe((res) => {
        if (res.errCode == 0) {
          this.nodata = false
          this.posts = res.response.data;
        }
        else {
          this.posts = null
          this.nodata = true
        }
      });
  }
  // deletePost(postId) {
  //   this.postsService.deletePost(postId)
  //     .subscribe((data) => {
  //       if (data.errCode == 0) {
  //         this.postsService.getAllActivePosts()
  //           .subscribe(data => {
  //             if (data.errCode == 0) {
  //               this.nodata = false
  //               this.posts = data.response.data;
  //             }
  //             else {
  //               this.posts = null
  //               this.nodata = true
  //             }
  //           });
  //       }
  //     })
  // }
  getUserById(userId) {
    this.postsService.getUserById(userId)
      .subscribe((data) => {
        console.log(data)
        this.user = data.response.data;
      })
  }
  // makeInactivePost(id) {
  //   this.postsService.makeInactivePost(id)
  //     .subscribe((data) => {
  //       if (data.errCode == 0) {
  //         this.postsService.getAllActivePosts()
  //           .subscribe(data => {
  //             if (data.errCode == 0) {
  //               this.nodata = false
  //               this.posts = data.response.data;
  //             }
  //             else {
  //               this.posts = null
  //               this.nodata = true
  //             }
  //           });
  //       }
  //     })
  // }

  getPostByIdToView(post) {
    this.postsService.getPostByIdToView(post)
      .subscribe((data) => {
        this.postByIdToView = data.response.data
        console.log(this.postByIdToView)
      })
  }
  goBack(): void {
    this._location.back();
  }
}
