import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { BaThemeSpinner, BaThemePreloader } from '../../../theme/services';
import { Location } from '@angular/common';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import { UsersService } from '../users.service';
import { AuthService } from '../../../sharedServices/authService/auth.service';
import { FileUploader } from 'ng2-file-upload';
const URL = API_URL+':8009/upload';

@Component({
  selector: 'edit',
  encapsulation: ViewEncapsulation.None,
  template: require('./edit.html')
})
export class EditUser implements OnInit {
  private addUserForm: FormGroup;
  private editUserForm: FormGroup;
  private userById: any;
  private id: string;
  users: any;
  private Imgage: any;
  private imageurl: any;
  searchControl = new FormControl();

  constructor(private route: ActivatedRoute,
    private router: Router, private _location: Location, private usersService: UsersService, private fb: FormBuilder,
    private authService: AuthService) {
  }

  ngOnInit() {
    let _self = this;
    this.route.params.forEach((params: Params) => {
      this.id = params['id'];
    })
    console.log(this.router)
    this.authService.checkCredentials();
    this.initEditFrom()
    this.usersService.getUserBy_Id(this.id)
      .subscribe((data) => {
        this.userById = data.response.data
        this.imageurl = this.userById.profilePic
        let control = this.editUserForm.controls
        control['_id'].setValue(this.userById._id)
        control['userId'].setValue(this.userById.userId)
        control['dob'].setValue(this.userById.dob)
        control['password'].setValue(this.userById.password)
        control['gender'].setValue(this.userById.gender)
        control['school'].setValue(this.userById.school)
        control['location'].setValue(this.userById.location)
        control['company'].setValue(this.userById.company)
        control['address'].setValue(this.userById.address)
        control['work'].setValue(this.userById.work)
        control['phone'].setValue(this.userById.phone)
        control['profilePic'].setValue(this.userById.profilePic)
        let emailcontrol = <FormGroup>this.editUserForm.controls['email'];
        emailcontrol.controls['primaryEmail'].setValue(this.userById.email.primaryEmail)
        emailcontrol.controls['workEmail'].setValue(this.userById.email.workEmail)
        let namecontrol = <FormGroup>this.editUserForm.controls['name'];
        namecontrol.controls['fName'].setValue(this.userById.name.fName)
        namecontrol.controls['lName'].setValue(this.userById.name.lName)
      })

    this.uploader.onSuccessItem = function (item: any, response: any, status: number, headers: any) {
      _self.Imgage = JSON.parse(response);
      _self.imageurl = (API_URL+":8009/" + _self.Imgage.url)
      console.log(_self.imageurl);
      _self.editUserForm.controls['profilePic'].setValue(_self.imageurl)
      return { item, response, status, headers };
    };
  }

  public uploader: FileUploader = new FileUploader({ url: URL, autoUpload: true, itemAlias: "photo" });
  public hasBaseDropZoneOver: boolean = false;
  public hasAnotherDropZoneOver: boolean = false;

  public fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }

  public fileOverAnother(e: any): void {
    this.hasAnotherDropZoneOver = e;
  }
  initEditFrom() {
    this.editUserForm = this.fb.group({
      "_id": '',
      "userId": [{ value: '', disabled: true }, Validators.compose([Validators.required])],
      "dob": ['', Validators.compose([Validators.required])],
      "password": ['', Validators.compose([Validators.required])],
      "gender": ['', Validators.compose([])],
      "about": ['', Validators.compose([])],
      "school": ['', Validators.compose([])],
      "location": ['', Validators.compose([])],
      "company": ['', Validators.compose([])],
      "address": ['', Validators.compose([])],
      "work": ['', Validators.compose([])],
      "phone": ['', Validators.compose([])],
      "profilePic": ['', Validators.compose([])],
      "email": this.fb.group({
        "primaryEmail": ['', Validators.compose([Validators.required])],
        "workEmail": ['', Validators.compose([])]
      }),
      "name": this.fb.group({
        "fName": ['', Validators.compose([Validators.required])],
        "lName": ['', Validators.compose([])]
      })
    })
  }

  editUser() {
    console.log(this.editUserForm.value)
    this.usersService.editUser(this.editUserForm.value)
      .subscribe((data) => {
        if (data.errCode == 0) {
          this._location.back();
        }
      })
  }
}
