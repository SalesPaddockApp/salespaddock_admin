import { Routes, RouterModule }  from '@angular/router';

import { Users } from './users.component';
import {Active} from './active/active.component';
import {InActive} from './inactive/inactive.component';
import {EditUser} from './edit/edit.component';
import {Posts} from './posts/posts.component';
import {Log} from './log/log.component';
// noinspection TypeScriptValidateTypes
const routes: Routes = [
  {
    path: '',
    component: Users,
    children: [
      { path: '', redirectTo: 'active', pathMatch: 'full' },
      { path: 'active', component: Active },
      { path: 'inactive', component: InActive }
    ]},
  { path: 'edit/:id', component: EditUser },
  { path: 'posts/:id', component: Posts},
  { path: 'log/:id', component: Log}
];

export const routing = RouterModule.forChild(routes);
