import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { BaThemeSpinner, BaThemePreloader } from '../../../theme/services';
import { Location } from '@angular/common';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import { UsersService } from '../users.service';
import { AuthService } from '../../../sharedServices/authService/auth.service';
import { FileUploader } from 'ng2-file-upload';
const URL = API_URL+':8009/upload';

@Component({
  selector: 'log',
  encapsulation: ViewEncapsulation.None,
  template: require('./log.html')
})
export class Log implements OnInit {
  private addUserForm: FormGroup;
  private editUserForm: FormGroup;
  private userById: any;
  private id: string;
  users: any;
  private Imgage: any;
  private imageurl: any;
  searchControl = new FormControl();
  private devices: any;
  private logg: any;

  constructor(private route: ActivatedRoute,
    private router: Router, private _location: Location, private usersService: UsersService, private fb: FormBuilder,
    private authService: AuthService) {
  }

  ngOnInit() {
    let _self = this;
    this.route.params.forEach((params: Params) => {
      this.id = params['id'];
      this.usersService.getDeviceLog(this.id)
      .subscribe((data)=>{
          console.log(data)
          if(data.errCode == 0){
              this.devices = data.response.data
          }
      })
    })
  }
  getToken(log){
    this.logg = log;
  }
}
