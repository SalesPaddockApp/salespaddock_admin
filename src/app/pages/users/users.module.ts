import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgaModule } from '../../theme/nga.module';
import { ReactiveFormsModule } from '@angular/forms';
import { routing }       from './users.routing';
import { Users } from './users.component';
import { UsersService } from './users.service';
import {Active} from './active/active.component';
import {Posts} from './posts/posts.component';
import {InActive} from './inactive/inactive.component';
import {EditUser} from './edit/edit.component';
import {Log} from './log/log.component';
import { PagesModule } from '../pages.module';
import { PostsService } from '../posts/posts.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgaModule,
    routing,
    ReactiveFormsModule,
    PagesModule
  ],
  declarations: [
    Users,
    Active,
    InActive,
    EditUser,
    Posts,
    Log
  ],
  providers: [
    UsersService,
    PostsService
  ]
})
export default class usersModule {}
