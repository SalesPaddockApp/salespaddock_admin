import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { BaThemeSpinner, BaThemePreloader } from '../../theme/services';
import { Location } from '@angular/common';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import { UsersService } from './users.service';
import { AuthService } from '../../sharedServices/authService/auth.service';

@Component({
  selector: 'users',
  encapsulation: ViewEncapsulation.None,
  template: `
    <router-outlet></router-outlet>
    `,
  styles: [require('./users.scss')]
})
export class Users{
  private addUserForm: FormGroup;
  private editUserForm: FormGroup;
  private userById: any;
  users: any;
  searchControl = new FormControl();

  constructor(private _location: Location, private usersService: UsersService, private fb: FormBuilder,
    private authService: AuthService) {
  }

}
