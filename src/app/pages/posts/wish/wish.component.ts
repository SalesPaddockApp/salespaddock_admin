import { Component, ViewEncapsulation, Input, ViewChild, OnInit, ElementRef, NgZone, ChangeDetectorRef } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl, FormArray } from '@angular/forms';
import { PostsService } from '../posts.service';
import { PrefsettingsService } from '../../producttypes/prefsettings/prefsettings.service';
import { ChekedFilterPipe } from '../../sharedServices/pipes/checkedPipe.ts';

@Component({
    selector: 'wish',
    encapsulation: ViewEncapsulation.None,
    template: require('./wish.html')
})
export class Wish {
    private id: string;
    public searchControl: FormControl;
    private data: any;
    private nodata: boolean = false;
    private ids: any[] = [];
    private notificationM: FormGroup;
    private success: boolean;
    private nosuccess: boolean;
    private allselected: boolean = false;

    constructor(private route: ActivatedRoute,
        private router: Router, private postsService: PostsService, private fb: FormBuilder, private prefsettingsService: PrefsettingsService, private zone: NgZone, private cdRef: ChangeDetectorRef, private _location: Location) {

    }

    ngOnInit() {
        this.route.params.forEach((params: Params) => {
            this.id = params['id'];
        });
        this.postsService.getWishListByPostId(this.id)
            .subscribe((data) => {
                console.log(data)
                if (data.errCode == 0) {
                    this.data = data.response.data
                }
                else {
                    this.nodata = true
                }
            })
        this.notificationM = this.fb.group({
            "message": ['', Validators.required]
        })
    }
    sendNotification() {
        this.postsService.sendNotification(this.notificationM.value, this.ids)
            .subscribe((data) => {
                if (data.errCode == 0) {
                    this.ids = []
                    this.success = true
                }
                else {
                    this.nosuccess = true
                }
            })

    }
    setIds(e, userId) {
        if (e.target.checked) {
            this.ids.push(userId)
        }
        else {
            console.log(this.ids.indexOf(userId))
            this.ids.splice(this.ids.indexOf(userId), 1);
        }
        console.log(this.ids)
    }
    selectAll(e){
        if(e.target.checked){
            this.data.forEach(data=>{
                this.ids.push(data.userId)
                this.allselected = true
            })
        }
        else{
            this.ids = []
            this.allselected = false
        }
        console.log(this.ids)
    }
}
