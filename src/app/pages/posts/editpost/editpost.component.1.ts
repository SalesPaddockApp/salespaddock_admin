import { Component, ViewEncapsulation, Input, ViewChild, OnInit, ElementRef, NgZone, ChangeDetectorRef } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl, FormArray } from '@angular/forms';
import { PostsService } from '../posts.service';
import { PrefsettingsService } from '../../producttypes/prefsettings/prefsettings.service';
import { ChekedFilterPipe } from '../../sharedServices/pipes/checkedPipe.ts';
import { FileUploader } from 'ng2-file-upload';
const URL = API_URL+':8009/upload';
import { ImageCropperComponent, CropperSettings } from 'ng2-img-cropper';

import { AgmCoreModule, MapsAPILoader } from 'angular2-google-maps/core';

@Component({
    selector: 'editpost',
    encapsulation: ViewEncapsulation.None,
    template: require('./editpost.html'),
    styles: [require('./editpost.scss')]
})
export class EditPostComponent {
    private streetName = "";
    private apt = "";
    private farmName = "";
    private city = "";
    private state = "";
    private zipCode = "";
    private farmID = "";
    private country = "";
    private farmDetailsData: any;
    private Imgage: any;
    private userId: any;
    private imageurl: any[] = [];
    private post: any;
    private producttypes: any;
    private id: string;
    private thisindex: any;
    private editPostForm: FormGroup;
    private prefsettings: any = [];
    private values: any[] = [];
    private rangeValue: any;
    public latitude: number;
    public longitude: number;
    public searchControl: FormControl;
    public zoom: number;
    cropperSettings: CropperSettings;
    private data: any;
    private changeImage: boolean = false;
    private mandatoryDataMissing: boolean = false;
    private valuesExist: boolean = false;
    private locationStatus: boolean = false;
    private cropper: boolean = false;
    private myDatePickerOptions: any;
    private uploaded: boolean = false;
    private price: any[] = [null, null]
    // private optionss: string;
    constructor(private route: ActivatedRoute,
        private router: Router, private postsService: PostsService, private fb: FormBuilder,
        private prefsettingsService: PrefsettingsService, private mapsAPILoader: MapsAPILoader,
        private zone: NgZone, private cdRef: ChangeDetectorRef, private _location: Location) {

        this.cropperSettings = new CropperSettings();
        this.cropperSettings.width = 810;
        this.cropperSettings.height = 540;
        this.cropperSettings.croppedWidth = 810;
        this.cropperSettings.croppedHeight = 540;
        this.cropperSettings.canvasWidth = 300;
        this.cropperSettings.canvasHeight = 200;
        this.data = {};
        this.myDatePickerOptions = {
            todayBtnTxt: 'Today',
            dateFormat: 'yyyy-mm-dd',
            firstDayOfWeek: 'mo',
            sunHighlight: true,
            height: '34px',
            width: '260px',
            inline: false,
            disableUntil: { year: 1800, month: 8, day: 10 },
            selectionTxtFontSize: '16px'
        };

    }

    ngOnInit() {
        this.route.params.forEach((params: Params) => {
            this.id = params['id'];
        });
        this.postsService.getPostByIdToView(this.id)
            .subscribe((data) => {
                this.post = data.response.data;
                this.initEditpostForm()
                this.userId = this.post.userId
                let value: any[] = this.post.values
                // this.prefsettingsService.getPrefToCreatePost(this.post.productId)
                //     .subscribe((data) => {
                //         console.log(data)
                //         if (data.errCode == 0) {
                //             this.prefsettings = data.response.data;
                //             console.log(this.prefsettings)
                //         }
                //     });

                this.prefsettings = value
                this.farmDetailsData = data.response.farmDetails ? data.response.farmDetails : [];
                value.forEach((data, index) => {
                    //  console.log('data2 '+JSON.stringify(data))
                    let control = <FormArray>this.editPostForm.controls['values']
                    control.push(this.editPostOptions(data));
                    this.imageurl.splice(index, 0, null)
                    if (data.typeOfPreference == 6) {
                        console.log('d.options:' + JSON.stringify(data.options))
                        if (data.options.length > 0) {
                            this.imageurl.splice(index, 0, data.options)
                        } else {
                            const uploader: FileUploader = new FileUploader({ url: URL, autoUpload: true, itemAlias: "photo" });
                        }

                    }
                    if (data.typeOfPreference == 8) {
                        this.imageurl.splice(index, 0, data.options)
                    }
                    // if (data.typeOfPreference == '6') {
                    //     const uploader: FileUploader = new FileUploader({ url: URL, autoUpload: true, itemAlias: "photo" });
                    // }
                    // else if (data.typeOfPreference == 4) {
                    //     if (data.preferenceTitle == 'Description') {
                    //         this.thisindex = index;
                    //         this.optionss = data.options[0]
                    //     }

                    // }
                    if (data.typeOfPreference == '7') {
                        this.thisindex = index;
                        //set google maps defaults
                        this.zoom = 20;
                        this.latitude = data.options ? parseFloat(data.options[0]['lat']) : this.latitude;
                        this.longitude = data.options ? parseFloat(data.options[0]['long']) : this.longitude;
                        this.city = data.options ? data.options[0]['city'] : "";
                        this.farmID = data.options ? data.options[0]['farmID'] : "";
                        this.farmName = data.options ? data.options[0]['farmName'] : "";
                        this.state = data.options ? data.options[0]['state'] : "";
                        this.zipCode = data.options ? data.options[0]['zipCode'] : 0;
                        this.country = data.options ? data.options[0]['country'] : "";
                        this.streetName = data.options ? data.options[0]['street'] : "";
                        this.apt = data.options ? data.options[0]['apt'] : "";

                        this.locationStatus = true;
                        //create search FormControl
                        this.searchControl = new FormControl();

                        this.cdRef.detectChanges();
                        // let searchBox: any = document.getElementById('search');
                        // console.log(searchBox);

                        // //load Places Autocomplete
                        // this.mapsAPILoader.load().then(() => {
                        //     let autocomplete = new google.maps.places.Autocomplete(searchBox, {

                        //     });
                        //     autocomplete.addListener("place_changed", () => {
                        //         //get the place result
                        //         let place: google.maps.places.PlaceResult = autocomplete.getPlace();
                        //         //set latitude and longitude
                        //         this.latitude = place.geometry.location.lat();
                        //         this.longitude = place.geometry.location.lng();
                        //         let mapvalue: any[] = [];
                        //         mapvalue.push(this.latitude);
                        //         mapvalue.push(this.longitude)
                        //         let control = <FormArray>this.editPostForm.controls['values']
                        //         let fg = <FormGroup>control.controls[thisindex]
                        //         fg.controls['options'].setValue(mapvalue)
                        //     });
                        // });

                    }
                })
                this.valuesExist = true; // added yun
            })
        this.postsService.getProducttypes()
            .subscribe((data) => {
                this.producttypes = data.response.data;
            });
    }
    // setCkeditorvalue(e, i) {
    //     const option = <FormArray>this.editPostForm.controls['values']
    //     const name = <FormGroup>option.controls[i]
    //     const options = name.controls['options']
    //     console.log(name)
    //     console.log(i)
    //     console.log(this.optionss)
    //     options.setValue([this.optionss])
    //     console.log(options.value)
    // }
    public uploader: FileUploader = new FileUploader({ url: URL, autoUpload: true, itemAlias: "photo" });
    public hasBaseDropZoneOver: boolean = false;
    public hasAnotherDropZoneOver: boolean = false;

    public fileOverBase(e: any): void {
        this.hasBaseDropZoneOver = e;
    }

    public fileOverAnother(e: any): void {
        this.hasAnotherDropZoneOver = e;
    }

    cropped(e, i) {
        console.log(this.data)
    }
    initMap() {    // added yun
        // myMap.triggerResize() // to resize map on popup
        //load Places Autocomplete
        this.mapsAPILoader.load().then(() => {
            let searchBox: any = document.getElementById('search');
            let autocomplete = new google.maps.places.Autocomplete(searchBox, {
            });
            autocomplete.addListener("place_changed", () => {
                //get the place result
                let place: google.maps.places.PlaceResult = autocomplete.getPlace();
                //set latitude and longitude
                this.latitude = place.geometry.location.lat();
                this.longitude = place.geometry.location.lng();

                this.postsService.getMapData(this.latitude, this.longitude)
                    .subscribe(location => {
                        if (location) {
                            //alert(JSON.stringify(location.results[0]))
                            console.log(location.results[0].formatted_address)
                            for (var i = 0; i < location.results[0].address_components.length; i++) {
                                for (var k = 0; k < location.results[0].address_components[i].types.length; k++) {
                                    if (location.results[0].address_components[i].types[k] == "route")
                                        this.streetName = location.results[0].address_components[i].short_name ? location.results[0].address_components[i].short_name : "";
                                    else if (location.results[0].address_components[i].types[k] == "locality")
                                        this.city = location.results[0].address_components[i].long_name ? location.results[0].address_components[i].long_name : "";
                                    else if (location.results[0].address_components[i].types[k] == "administrative_area_level_1")
                                        this.state = location.results[0].address_components[i].long_name ? location.results[0].address_components[i].long_name : ""
                                    else if (location.results[0].address_components[i].types[k] == "postal_code")
                                        this.zipCode = location.results[0].address_components[i].long_name ? location.results[0].address_components[i].long_name : "";
                                    else if (location.results[0].address_components[i].types[k] == "country")
                                        this.country = location.results[0].address_components[i].long_name ? location.results[0].address_components[i].long_name : "";
                                }
                            }
                            this.farmName = ""
                            this.apt = ""
                            this.streetName = ""
                            this.farmID = getRandom(16)
                            let mapvalue: any[] = [{ "farmID": this.farmID, "long": this.longitude, "lat": this.latitude, city: this.city, state: this.state, country: this.country, zipCode: this.zipCode, farmName: this.farmName, apt: this.apt, street: this.streetName }];
                            let control = <FormArray>this.editPostForm.controls['values']
                            let fg = <FormGroup>control.controls[this.thisindex]
                            fg.controls['options'].setValue(mapvalue)

                        } else {

                            let mapvalue: any[] = [{ "farmID": this.farmID ? this.farmID : getRandom(16), "long": this.longitude, "lat": this.latitude }];
                            let control = <FormArray>this.editPostForm.controls['values']
                            let fg = <FormGroup>control.controls[this.thisindex]
                            fg.controls['options'].setValue(mapvalue)
                        }
                    });




                // let mapvalue: any[] = []; //  error resolved yunus
                //  mapvalue.push(this.latitude);
                // mapvalue.push(this.longitude)
                // let control = <FormArray>this.editPostForm.controls['values']
                // let fg = <FormGroup>control.controls[this.thisindex]
                // fg.controls['options'].setValue(mapvalue)
            });
        });
    }
    chngImage() {
        this.changeImage = true
        this.cropper = true
    }
    confirmUploadNew(i) {
        console.log(this.data.image)
        let dataURI = this.data.image;
        function dataURItoBlob(dataURI: string) {
            // convert base64 to raw binary data held in a string
            // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
            var byteString = atob(dataURI.split(',')[1]);

            // separate out the mime component
            var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]

            // write the bytes of the string to an ArrayBuffer
            var ab = new ArrayBuffer(byteString.length);
            var ia = new Uint8Array(ab);
            for (var i = 0; i < byteString.length; i++) {
                ia[i] = byteString.charCodeAt(i);
            }

            // write the ArrayBuffer to a blob, and you're done
            var blob = new Blob([ab], { type: mimeString });
            return blob;

            // Old code
            // var bb = new BlobBuilder();
            // bb.append(ab);
            // return bb.getBlob(mimeString);
        }
        var blob = dataURItoBlob(dataURI);
        var fileName = (Math.random().toString(36).substring(7) + ".png");
        let imgagefile = new File([blob], fileName)
        console.log(blob)
        this.makeFileRequest(URL, [], imgagefile).then((result) => {
            console.log("result['url']", JSON.stringify(result['url']));
            const url = result['url'];
            const option = <FormArray>this.editPostForm.controls['values']
            const name = <FormGroup>option.controls[i]
            let options = name.controls['options']
            options.setValue([{ imageUrl: API_URL+"/" + url, type: "0" }])
            console.log(options)
            this.imageurl[i] = API_URL+"/" + url;

            console.log(this.imageurl[i]);

            this.uploaded = true
        })
    }
    confirmUpload(i, j) {
        console.log(this.data.image)
        let dataURI = this.data.image;
        function dataURItoBlob(dataURI: string) {
            // convert base64 to raw binary data held in a string
            // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
            var byteString = atob(dataURI.split(',')[1]);

            // separate out the mime component
            var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]

            // write the bytes of the string to an ArrayBuffer
            var ab = new ArrayBuffer(byteString.length);
            var ia = new Uint8Array(ab);
            for (var i = 0; i < byteString.length; i++) {
                ia[i] = byteString.charCodeAt(i);
            }

            // write the ArrayBuffer to a blob, and you're done
            var blob = new Blob([ab], { type: mimeString });
            return blob;

            // Old code
            // var bb = new BlobBuilder();
            // bb.append(ab);
            // return bb.getBlob(mimeString);
        }
        var blob = dataURItoBlob(dataURI);
        var fileName = (Math.random().toString(36).substring(7) + ".png");
        let imgagefile = new File([blob], fileName)
        console.log(blob)
        this.makeFileRequest(URL, [], imgagefile).then((result) => {
            console.log(JSON.stringify(result['url']));
            const url = result['url'];
            const option = <FormArray>this.editPostForm.controls['values']
            const name = <FormGroup>option.controls[i]
            let options = name.controls['options'].value
            console.log(options)
            for (var index = 0; index < options.length; index++) {
                console.log("options " + JSON.stringify(options[index]))
                if (options[index].imageUrl == j && options[index].type == 1) {
                    options[index].imageUrl = API_URL+"/" + url
                    options[index].thumbnailUrl = API_URL+"/" + url + ".png"
                }
                else if (options[index].imageUrl == j && options[index].type == 0) {
                    options[index].imageUrl = API_URL+"/" + url
                }

            }
            // console.log("image URL "+ imgUrl);
            // imgUrl.setValue(API_URL+":8009/" + url)
            // console.log(options.value)
            // this.imageurl[i] = options.value
            console.log(this.imageurl)
            this.uploaded = true
        })
    }

    initEditpostForm() {
        this.editPostForm = this.fb.group({
            "_id": this.post._id,
            "userId": [{ value: this.post.userId, disabled: true }, Validators.compose([
                Validators.required])],
            "productType": [{ value: this.post.productType, disabled: true }, Validators.compose([
                Validators.required])],
            "values": this.fb.array([
            ])
        })
    }

    editPostOptions(val) {
        return this.fb.group({
            "options": [val.options, Validators.compose([
                Validators.required])],
            "pref_id": [val.pref_id, Validators.compose([
                Validators.required])]
        })
    }

    addOptions() {
        var val = ''
        const control = <FormArray>this.editPostForm.controls['optionsValue'];
        control.push(this.editPostOptions(val));
    }

    deleteOptions(i: number) {
        const control = <FormArray>this.editPostForm.controls['optionsValue'];
        control.removeAt(i)
    }
    farmData(data) {
        return data.farmName + ",  " + data.street + ",  " + data.city + ",  " + data.state + ",  " + data.country + " - " + data.zipCode;
    }
    selectFarmData(e) {
        if (e) {
            var data = JSON.parse(e)
            this.farmName = data.farmName
            this.farmID = data.farmID
            this.apt = data.apt
            this.streetName = data.street
            this.zipCode = data.zipCode
            this.city = data.city
            this.state = data.state
            this.country = data.country
            this.latitude = data.lat  // for change map
            this.longitude = data.long // for change map
        }
    }
    editPost(locationExists) {
        if (locationExists == 0) {
            let values: any[] = []
            values = this.editPostForm.value.values;
            console.log(values)
            values.forEach((data, i) => {
                console.log(data)
                if (typeof data.options == 'string') {
                    this.editPostForm.value.values[i].options = [data.options]
                }
                else {
                    this.editPostForm.value.values[i].options = data.options
                }
            })
            console.log(this.editPostForm.value)
            this.postsService.editPost(this.editPostForm.value)
                .subscribe((data) => {
                    if (data.errCode == 0) {
                        this._location.back();
                    }
                    else {
                        alert(data.Message);
                    }
                })
        } else {
            console.log(this.editPostForm.value)
            if (!this.streetName || this.streetName == "" || !this.farmName || this.farmName == "" || this.city == "" || !this.city || !this.state || this.state == "" || !this.country || this.country == "" || !this.zipCode || this.zipCode == "") {
                // alert('Mandatory fields missing in location')
                this.mandatoryDataMissing = true
                setTimeout(function () {
                    this.mandatoryDataMissing = false
                }, 3000);
            } else {
                let mapvalue: any[] = [{ "farmID": this.farmID ? this.farmID : getRandom(16), "long": this.longitude, "lat": this.latitude, city: this.city, state: this.state, country: this.country, zipCode: this.zipCode, farmName: this.farmName, street: this.streetName, apt: this.apt }];
                let control = <FormArray>this.editPostForm.controls['values']
                let fg = <FormGroup>control.controls[this.thisindex]
                fg.controls['options'].setValue(mapvalue) // added yun
                let values: any[] = []
                values = this.editPostForm.value.values;
                this.editPostForm.value.farmDetails = mapvalue[0]   // added to push in users location object
                this.editPostForm.value.userId = this.userId
                console.log(values)
                values.forEach((data, i) => {
                    console.log(data)
                    if (typeof data.options == 'string') {
                        this.editPostForm.value.values[i].options = [data.options]
                    }
                    else {
                        this.editPostForm.value.values[i].options = data.options
                    }
                })
                console.log(this.editPostForm.value)
                this.postsService.editPost(this.editPostForm.value)
                    .subscribe((data) => {
                        if (data.errCode == 0) {
                            this._location.back();
                        }
                        else {
                            alert(data.Message);
                        }
                    })
            }
        }
    }

    setMandatory(e) {
        if (e.target.checked) {
            console.log(this.editPostForm.controls['mandatory'].value)
            this.editPostForm.controls['mandatory'].setValue(1)
            console.log(this.editPostForm.controls['mandatory'].value)
        }
        else {
            console.log(this.editPostForm.controls['mandatory'].value)
            this.editPostForm.controls['mandatory'].setValue(0);
            console.log(this.editPostForm.controls['mandatory'].value)

        }
    }

    setOCS(e) {
        if (e.target.checked) {
            const control = this.editPostForm.controls['onCreateScreen']
            // this.editPostForm.controls['oneditScreen'].patchValue(1);
            control.patchValue(1)
        }
        else {
            const control = this.editPostForm.controls['onCreateScreen']
            control.patchValue(0)
            // this.editPostForm.controls['oneditScreen'].patchValue(0);
        }

    }

    setRadioValue(e, i) {
        const option = <FormArray>this.editPostForm.controls['values']
        const name = <FormGroup>option.controls[i]
        const options = name.controls['options']
        console.log(name)
        console.log(i)
        console.log(e.target.value)
        options.setValue([e.target.value])
        console.log(options.value)

    }

    setSaleValue(e, i, opt) {
        console.log(e)
        this.price[0] = e.target.value
        const option = <FormArray>this.editPostForm.controls['values']
        const name = <FormGroup>option.controls[i]
        const options = name.controls['options']
        options.setValue(this.price);
        console.log("Slider value " + options.value)
    }

    setLeaseValue(e, i, opt) {
        this.price[1] = e.target.value
        const option = <FormArray>this.editPostForm.controls['values']
        const name = <FormGroup>option.controls[i]
        const options = name.controls['options']
        options.setValue(this.price);
        console.log("Slider value " + options.value)
    }

    setCheckValue(e, i, opt) {
        const option = <FormArray>this.editPostForm.controls['values']
        const name = <FormGroup>option.controls[i]
        const options = name.controls['options']
        if (e.target.checked) {
            console.log("checked")
            if (options.value) {
                this.values = options.value
                this.values.push(opt)
            }
            else {
                this.values.push(opt)
            }
            options.setValue(this.values)
        }
        else {
            console.log("unchecked")
            if (options.value) {
                this.values = options.value
                this.values.splice(this.values.indexOf(opt), 1)
            }
            // else {
            //     this.values.splice(this.values.indexOf(opt), 1)
            // }
            options.setValue(this.values)
        }
        console.log(options.value)
        this.values = []
    }

    setSliderValue(e, i, opt) {
        this.rangeValue = e.target.value
        console.log(e.target.value)
        const option = <FormArray>this.editPostForm.controls['values']
        const name = <FormGroup>option.controls[i]
        const options = name.controls['options']
        options.setValue([e.target.value]);
        console.log("Slider value " + options.value)
    }

    onDateChanged(e, i) {
        console.log(e)
        const option = <FormArray>this.editPostForm.controls['values']
        const name = <FormGroup>option.controls[i]
        const options = name.controls['options']
    }

    filesToUpload: Array<File>;

    upload(i, j) {
        this.makeFileRequest(URL, [], this.filesToUpload[0]).then((result) => {
            console.log(JSON.stringify(result['url']));
            const url = result['url'];
            const option = <FormArray>this.editPostForm.controls['values']
            const name = <FormGroup>option.controls[i]
            let options = name.controls['options'].value
            console.log(options)
            for (var index = 0; index < options.length; index++) {
                console.log("options " + JSON.stringify(options[index]))
                if (options[index].imageUrl == j && options[index].type == 1) {
                    options[index].imageUrl = API_URL+"/" + url
                    options[index].thumbnailUrl = API_URL+"/" + url + ".png"
                }
                else if (options[index].imageUrl == j && options[index].type == 0) {
                    options[index].imageUrl = API_URL+"/" + url
                }

            }
            // console.log("image URL "+ imgUrl);
            // imgUrl.setValue(API_URL+":8009/" + url)
            // console.log(options.value)
            // this.imageurl[i] = options.value
            console.log(this.imageurl)
        },
            (error) => {
                console.error(error);
            });
    }

    fileChangeEvent(fileInput: any, i, j) {
        this.filesToUpload = <Array<File>>fileInput.target.files;
        this.upload(i, j);
        const name = this.filesToUpload[0]
        console.log(this.filesToUpload[0].name)
    }

    makeFileRequest(url: string, params: Array<string>, files: any) {
        return new Promise((resolve, reject) => {
            var formData: any = new FormData();
            var xhr = new XMLHttpRequest();
            // for (var i = 0; i < files.length; i++) {
            //     formData.append("uploads[]", files[i], files[i].name);
            //     console.log(formData.value)
            // }
            formData.append("photo", files, files.name)
            console.log(formData)
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4) {
                    if (xhr.status == 200) {
                        resolve(JSON.parse(xhr.response));
                    } else {
                        reject(xhr.response);
                    }
                }
            }
            xhr.open("POST", url, true);
            xhr.send(formData);
        });
    }

    fileChange(e) {
        console.log(this.uploader)
    }
}
function getRandom(length) {

    return Math.floor(Math.pow(10, length - 1) + Math.random() * 9 * Math.pow(10, length - 1)).toString();

}