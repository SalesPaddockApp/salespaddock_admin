import { Component, ViewEncapsulation, Input, ViewChild, OnInit, ElementRef, NgZone, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl, FormArray } from '@angular/forms';
import { PostsService } from '../posts.service';
import { PrefsettingsService } from '../../producttypes/prefsettings/prefsettings.service';
import { ChekedFilterPipe } from '../../sharedServices/pipes/checkedPipe.ts';
import { FileUploader } from 'ng2-file-upload';
const URL = API_URL+':8009/upload';

import { AgmCoreModule, MapsAPILoader } from 'angular2-google-maps/core';

@Component({
    selector: 'view',
    encapsulation: ViewEncapsulation.None,
    template: require('./view.html'),
    styles: [require('./view.scss')]
})
export class ViewPostComponent {
    private Imgage: any;
    private imageurl: any[] = [];
    private post: any;
    private producttypes: any;
    private id: string;
    private viewForm: FormGroup;
    private prefsettings: any = [];
    private values: any[] = [];
    private rangeValue: any;
    public latitude: number;
    public longitude: number;
    public searchControl: FormControl;
    public zoom: number;
    constructor(private route: ActivatedRoute,
        private router: Router, private postsService: PostsService, private fb: FormBuilder, private prefsettingsService: PrefsettingsService, private mapsAPILoader: MapsAPILoader, private zone: NgZone, private cdRef: ChangeDetectorRef) {
    }

    ngOnInit() {
        this.route.params.forEach((params: Params) => {
            this.id = params['id'];
        });
        this.postsService.getPostByIdToView(this.id)
            .subscribe((data) => {
                console.log(data)
                this.post = data.response.data;
                this.initviewForm()
                let values: any[] = this.post.values
                console.log(values)
                this.prefsettings = values
                values.forEach((data, index) => {
                    let control = <FormArray>this.viewForm.controls['values']
                    control.push(this.viewOptions(data));
                    this.imageurl.splice(index, 0, null)
                    if (data.typeOfPreference == 6 || data.typeOfPreference == 8) {
                        console.log(index)
                        this.imageurl.splice(index, 0, data.options)
                        console.log(this.imageurl)
                    }
                    else if (data.typeOfPreference == '7') {
                        let thisindex = index;
                        //set google maps defaults
                        this.zoom = 20;
                        this.latitude = data.options[0];
                        this.longitude = data.options[1];

                        //create search FormControl
                        this.searchControl = new FormControl();

                        this.cdRef.detectChanges();
                        let searchBox: any = document.getElementById('search');
                        console.log(searchBox);

                    }
                })
                console.log(this.post)
            })
        this.postsService.getProducttypes()
            .subscribe((data) => {
                this.producttypes = data.response.data;
            });


    }

    public uploader: FileUploader = new FileUploader({ url: URL, autoUpload: true, itemAlias: "photo" });
    public hasBaseDropZoneOver: boolean = false;
    public hasAnotherDropZoneOver: boolean = false;

    public fileOverBase(e: any): void {
        this.hasBaseDropZoneOver = e;
    }

    public fileOverAnother(e: any): void {
        this.hasAnotherDropZoneOver = e;
    }


    initviewForm() {
        this.viewForm = this.fb.group({
            "_id": this.post._id,
            "userId": [{ value: this.post.userId, disabled: true }, Validators.compose([
                Validators.required])],
            "productType": [{ value: this.post.productType, disabled: true }, Validators.compose([
                Validators.required])],
            "values": this.fb.array([
            ])
        })
    }
    viewOptions(val) {
        return this.fb.group({
            "options": [val.options, Validators.compose([
                Validators.required])],
            "pref_id": [val.pref_id, Validators.compose([
                Validators.required])]
        })
    }
    addOptions() {
        var val = ''
        const control = <FormArray>this.viewForm.controls['optionsValue'];
        control.push(this.viewOptions(val));
    }

    deleteOptions(i: number) {
        const control = <FormArray>this.viewForm.controls['optionsValue'];
        control.removeAt(i)
    }
    // view() {
    //     console.log(this.viewForm.value)
    //     let values: any[] = []
    //     values = this.viewForm.value.values;
    //     console.log(values)
    //     values.forEach((data, i) => {
    //         console.log(data)
    //         if (typeof data.options == 'string') {
    //             this.viewForm.value.values[i].options = [data.options]
    //         }
    //         else {
    //             this.viewForm.value.values[i].options = data.options
    //         }
    //     })
    //     this.postsService.view(this.viewForm.value)
    //         .subscribe((data) => {
    //             if (data.errCode == 0) {
    //                 this.router.navigate(['pages/posts'])
    //             }
    //             else {
    //                 alert(data.Message);
    //             }
    //         })

    // }
    setMandatory(e) {
        if (e.target.checked) {
            console.log(this.viewForm.controls['mandatory'].value)
            this.viewForm.controls['mandatory'].setValue(1)
            console.log(this.viewForm.controls['mandatory'].value)
        }
        else {
            console.log(this.viewForm.controls['mandatory'].value)
            this.viewForm.controls['mandatory'].setValue(0);
            console.log(this.viewForm.controls['mandatory'].value)

        }
    }
    setOCS(e) {
        if (e.target.checked) {
            const control = this.viewForm.controls['onCreateScreen']
            // this.viewForm.controls['oneditScreen'].patchValue(1);
            control.patchValue(1)
        }
        else {
            const control = this.viewForm.controls['onCreateScreen']
            control.patchValue(0)
            // this.viewForm.controls['oneditScreen'].patchValue(0);
        }

    }

    setRadioValue(e, i) {
        const option = <FormArray>this.viewForm.controls['values']
        const name = <FormGroup>option.controls[i]
        const options = name.controls['options']
        console.log(name)
        console.log(i)
        console.log(e.target.value)
        options.setValue([e.target.value])
        console.log(options.value)

    }
    setCheckValue(e, i, opt) {
        const option = <FormArray>this.viewForm.controls['values']
        const name = <FormGroup>option.controls[i]
        const options = name.controls['options']
        if (e.target.checked) {
            console.log("checked")
            if (options.value) {
                this.values = options.value
                this.values.push(opt)
            }
            else {
                this.values.push(opt)
            }
            options.setValue(this.values)
        }
        else {
            console.log("unchecked")
            if (options.value) {
                this.values = options.value
                this.values.splice(this.values.indexOf(opt), 1)
            }
            // else {
            //     this.values.splice(this.values.indexOf(opt), 1)
            // }
            options.setValue(this.values)
        }
        console.log(options.value)
        this.values = []
    }
    setSliderValue(e, i, opt) {
        this.rangeValue = e.target.value
        console.log(e.target.value)
        const option = <FormArray>this.viewForm.controls['values']
        const name = <FormGroup>option.controls[i]
        const options = name.controls['options']
        options.setValue([e.target.value]);
        console.log("Slider value " + options.value)
    }

    filesToUpload: Array<File>;

    upload(i) {
        this.makeFileRequest(URL, [], this.filesToUpload[0]).then((result) => {
            console.log(JSON.stringify(result['url']));
            const url = result['url'];
            const option = <FormArray>this.viewForm.controls['values']
            const name = <FormGroup>option.controls[i]
            const options = name.controls['options']
            options.setValue([API_URL+":8009/" + url])
            console.log(options.value)
            this.imageurl[i] = options.value
            console.log(this.imageurl)
        },
            (error) => {
                console.error(error);
            });
    }

    fileChangeEvent(fileInput: any, i) {
        this.filesToUpload = <Array<File>>fileInput.target.files;
        this.upload(i);
        const name = this.filesToUpload[0]
        console.log(this.filesToUpload[0].name)
    }

    makeFileRequest(url: string, params: Array<string>, files: any) {
        return new Promise((resolve, reject) => {
            var formData: any = new FormData();
            var xhr = new XMLHttpRequest();
            // for (var i = 0; i < files.length; i++) {
            //     formData.append("uploads[]", files[i], files[i].name);
            //     console.log(formData.value)
            // }
            formData.append("photo", files, files.name)
            console.log(formData)
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4) {
                    if (xhr.status == 200) {
                        resolve(JSON.parse(xhr.response));
                    } else {
                        reject(xhr.response);
                    }
                }
            }
            xhr.open("POST", url, true);
            xhr.send(formData);
        });
    }
    fileChange(e) {
        console.log(this.uploader)
    }
}
