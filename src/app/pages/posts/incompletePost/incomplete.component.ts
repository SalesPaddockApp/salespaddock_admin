import { Component, OnInit } from '@angular/core';

import { Location } from '@angular/common';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import { PostsService } from '../posts.service';
import { AuthService } from '../../../sharedServices/authService/auth.service';

@Component({
  selector: 'incomplete',
  template: require('./incomplete.html')
})
export class IncompletePost implements OnInit { 
  private editpostForm: FormGroup;
  private postById: any;
  private nodata: boolean = false;
  posts: any;
  private postByIdToView: any;
  private user: any;
  searchControl = new FormControl();
  private ids: any[] = [];

  private moreData: any[];
  private pageNum: number = 0;
  private disable = false;
  private nouserresult: any;
  constructor(private _location: Location, private postsService: PostsService, private fb: FormBuilder,
    private authService: AuthService, private router: Router) {
  }

  ngOnInit() {
    this.authService.checkCredentials();
    this.getAllIncomletePosts();
    this.searchControl.valueChanges
      .debounceTime(400)
      .distinctUntilChanged()
      .flatMap(seachControl => this.postsService.searchIncompletePost(seachControl))
      .subscribe(data => {
        if (data.errCode == 0) {
          this.posts = data.response.data
          this.disable = true
          this.nouserresult = false
        }
        else if (data.errNum == 132) {
          this.pageNum = 0;
          this.nouserresult = false
          this.postsService.getAllIncomletePosts(this.pageNum)
            .subscribe((res) => {
              if (res.errCode == 0) {
                this.nodata = false
                this.disable = false
                this.posts = res.response.data;
                if (this.posts.length % 10 != 0)
                  this.disable = true;
              }
              else {
                this.nodata = true
              }
            });
        }
        else {
          this.posts = null;
          this.nouserresult = true
        }
      });
  }
  loadMore() {
    this.pageNum = this.pageNum + 1;
    this.postsService.getAllIncomletePosts(this.pageNum)
      .subscribe(res => {
        console.log(res)
        this.moreData = res.response.data;
        if (res.errCode == 0) {
          let len = this.moreData.length;
          for (let i = 0; i < len; i++)
            this.posts.push(this.moreData[i]);
        }
        if (this.posts.length % 10 != 0)
          this.disable = true;
      });
  }

  getAllIncomletePosts() {
    this.postsService.getAllIncomletePosts(this.pageNum)
      .subscribe((res) => {
        console.log(res);
        if (res.errCode == 0) {
          this.nodata = false
          this.posts = res.response.data;
          if (this.posts.length % 10 != 0)
              this.disable = true;
        }
        else {
          this.posts = null
          this.nodata = true
        }
      });
  }

  editPost() {
    this.router.navigate(['/pages/posts/edit', this.ids[0]]);
  }

  viewPost() {
    this.router.navigate(['/pages/posts/view', this.ids[0]]);
  }
  
  deletePost() {
    this.postsService.deletePost(this.ids)
      .subscribe((data) => {
        if (data.errCode == 0) {
          this.postsService.getAllIncomletePosts(this.pageNum)
            .subscribe(data => {
              if (data.errCode == 0) {
                this.nodata = false
                this.posts = data.response.data;
              }
              else {
                this.posts = null
                this.nodata = true
              }
            });
        }
      })
  }

  getUserById(userId) {
    this.postsService.getUserById(userId)
      .subscribe((data) => {
        this.user = data.response.data;
      })
  }

  makeActivePost(id) {
    this.postsService.makeActivePost(id)
      .subscribe((data) => {
        if (data.errCode == 0) {
          this.postsService.getAllInActivePosts(this.pageNum)
            .subscribe(data => {
              if (data.errCode == 0) {
                this.nodata = false
                this.posts = data.response.data;
              }
              else {
                this.posts = null
                this.nodata = true
              }
            });
        }
      })
  }
  getPostByIdToView(post) {
    this.postsService.getPostByIdToView(post)
      .subscribe((data) => {
        this.postByIdToView = data.response.data
        console.log(this.postByIdToView)
      })
  }
  setIds(e, producttype) {
    console.log(producttype)
    if (e.target.checked) {
      this.ids.push(producttype._id)
      this.postById = producttype
    }
    else {
      this.ids.splice(this.ids.indexOf(producttype._id));
    }
    console.log(this.ids)
  }
}
