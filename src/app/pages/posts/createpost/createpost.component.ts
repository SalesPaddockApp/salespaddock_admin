import { Component, ViewEncapsulation, Input, ViewChild, OnInit, ElementRef, NgZone, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl, FormArray } from '@angular/forms';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import { PostsService } from '../posts.service';
import { PrefsettingsService } from '../../producttypes/prefsettings/prefsettings.service';
import { FileUploader } from 'ng2-file-upload';
const URL = API_URL+':8009/upload';
import { AgmCoreModule, MapsAPILoader } from 'angular2-google-maps/core';
import { ImageCropperComponent, CropperSettings } from 'ng2-img-cropper';

@Component({
    selector: 'createpost',
    encapsulation: ViewEncapsulation.None,
    template: require('./createpost.html'),
    styles: [require('./createpost.scss')]
})
export class CreatePostComponent {
    private streetNumber = "";
    private apt = "";
    private farmName = "";
    private city = "";
    private state = "";
    private zip = "";
    private farmId = "";
    private country = "";
    private fulladdress: any;
    private farmDetailsData: any;
    private Imgage: any;
    private thisindex: any;
    private imageurl: any[] = [null];
    private prefsettings: any = [];
    private values: any[] = [];
    private producttypes: any[];
    private producttype: any;
    private msisdn: string;
    private TOP: any;
    private rangeValue: any;
    private users: any;
    private createPostForm: FormGroup;
    private noprefsetting: boolean = false;
    private mandatoryDataMissing: boolean = false;
    private novalues: boolean = false;
    public latitude: any = 0;
    public longitude: any = 0;
    searchControl = new FormControl();
    public zoom: number;
    private data: any;
    private uploaded: boolean = false;
    private price: any[] = [null, null];
    private userselected: boolean = false;
    private selecteduser: any;

    cropperSettings: CropperSettings;

    private myDatePickerOptions: any;
    // @ViewChild("search")
    // public searchElementRef: ElementRef;

    constructor(private route: ActivatedRoute,
        private router: Router, private postsService: PostsService, private fb: FormBuilder, private prefsettingsService: PrefsettingsService, private mapsAPILoader: MapsAPILoader, private zone: NgZone, private cdRef: ChangeDetectorRef) {
        this.filesToUpload = [];
        this.cropperSettings = new CropperSettings();
        this.cropperSettings.width = 810;
        this.cropperSettings.height = 540;
        this.cropperSettings.croppedWidth = 810;
        this.cropperSettings.croppedHeight = 540;
        this.cropperSettings.canvasWidth = 300;
        this.cropperSettings.canvasHeight = 200;

        this.data = {};

        this.myDatePickerOptions = {
            todayBtnTxt: 'Today',
            dateFormat: 'yyyy-mm-dd',
            firstDayOfWeek: 'mo',
            sunHighlight: true,
            height: '34px',
            width: '260px',
            inline: false,
            disableUntil: { year: 1800, month: 8, day: 10 },
            selectionTxtFontSize: '16px'
        };
    }

    ngOnInit() {
        let _self = this;
        this.postsService.getUsers()
            .subscribe((data) => {
                console.log(data)
                this.users = data.response.data;
            });

        this.searchControl.valueChanges
            .debounceTime(400)
            .distinctUntilChanged()
            .flatMap(seachControl => this.postsService.searchUser(seachControl))
            .subscribe(data => {
                console.log(data)
                if (data.errCode == 0) {
                    this.users = data.response.data
                }
                else if (data.errNum == 132) {
                    this.postsService.getUsers()
                        .subscribe((data) => {
                            console.log(data)
                            this.users = data.response.data;
                        });
                }
                else {
                    this.users = null;
                }
            });


    }
    ngAfterContentInit() {

    }
    public uploader: FileUploader = new FileUploader({ url: URL, autoUpload: true, itemAlias: "photo" });
    public hasBaseDropZoneOver: boolean = false;
    public hasAnotherDropZoneOver: boolean = false;

    public fileOverBase(e: any): void {
        this.hasBaseDropZoneOver = e;
    }

    public fileOverAnother(e: any): void {
        this.hasAnotherDropZoneOver = e;
    }

    selectUser(e, user) {
        console.log(user)
        if (e.target) {
            this.userselected = true
            this.selecteduser = user
            this.initAddPostsettingForm()

            this.postsService.getProducttypes()
                .subscribe((data) => {
                    console.log(data)
                    this.producttypes = data.response.data;
                    for (var index = 0; index < this.producttypes.length; index++) {
                        if (this.producttypes[index].productName == "Horse") {
                            this.selectProductType(this.producttypes[index]._id, user.userId)
                        }
                    }
                });
        }
    }


    selectProductType(e, userId) {
        this.prefsettingsService.getPrefToCreatePost(e, userId)
            .subscribe((data) => {
                console.log(data)
                this.initAddPostsettingForm()
                this.createPostForm.controls['productId'].setValue(e)
                this.prefsettingsService.getProducttypeById(e)
                    .subscribe((data) => {
                        this.producttype = data.response.data.name
                        this.createPostForm.controls['productType'].setValue(this.producttype)
                    })
                if (data.errCode == 0) {
                    this.novalues = false
                    this.noprefsetting = false;
                    this.prefsettings = data.response.data;
                    this.farmDetailsData = data.response.farmDetails ? data.response.farmDetails : [];
                    let control = <FormArray>this.createPostForm.controls['values']
                    this.prefsettings.forEach((data, index) => {
                        control.push(this.fb.group({
                            "options": ['', Validators.compose([
                                Validators.required])],
                            "pref_id": [data._id, Validators.compose([
                                Validators.required])]
                        }))
                        if (data.typeOfPreference == '6') {
                            const uploader: FileUploader = new FileUploader({ url: URL, autoUpload: true, itemAlias: "photo" });
                        }
                        else if (data.typeOfPreference == '7') {
                            //  let thisindex = index;
                            this.thisindex = index;
                            //set google maps defaults
                            this.zoom = 20;
                            this.latitude = 39.8282;
                            this.longitude = -98.5795;

                            //create search FormControl
                            this.searchControl = new FormControl();

                            //set current position
                            if ("geolocation" in navigator) {
                                navigator.geolocation.getCurrentPosition((position) => {
                                    this.latitude = position.coords.latitude;
                                    this.longitude = position.coords.longitude;
                                    this.zoom = 20;
                                    let mapvalue: any[] = [{ "long": this.longitude, "lat": this.latitude }];
                                    // mapvalue.push(this.latitude);
                                    // mapvalue.push(this.longitude)
                                    let control = <FormArray>this.createPostForm.controls['values'];
                                    let fg = <FormGroup>control.controls[this.thisindex]
                                    fg.controls['options'].setValue(mapvalue);

                                    console.log("mapvalue", JSON.stringify(mapvalue));
                                });
                            }
                            this.cdRef.detectChanges();
                            let searchBox: any = document.getElementById('search');
                            console.log(searchBox);

                            //load Places Autocomplete
                            this.mapsAPILoader.load().then(() => {
                                let autocomplete = new google.maps.places.Autocomplete(searchBox, {

                                });
                                autocomplete.addListener("place_changed", () => {
                                    //get the place result
                                    let place: google.maps.places.PlaceResult = autocomplete.getPlace();
                                    //set latitude and longitude
                                    this.latitude = place.geometry.location.lat();
                                    this.longitude = place.geometry.location.lng();

                                    this.postsService.getMapData(this.latitude, this.longitude)
                                        .subscribe(location => {
                                            if (location) {
                                                //alert(JSON.stringify(location.results[0]))
                                                console.log(location.results[0].formatted_address)
                                                for (var i = 0; i < location.results[0].address_components.length; i++) {
                                                    for (var k = 0; k < location.results[0].address_components[i].types.length; k++) {

                                                        if (location.results[0].address_components[i].types[k] == "locality")
                                                            this.city = location.results[0].address_components[i].long_name;
                                                        else if (location.results[0].address_components[i].types[k] == "administrative_area_level_1")
                                                            this.state = location.results[0].address_components[i].long_name
                                                        else if (location.results[0].address_components[i].types[k] == "postal_code")
                                                            this.zip = location.results[0].address_components[i].long_name;
                                                        else if (location.results[0].address_components[i].types[k] == "country")
                                                            this.country = location.results[0].address_components[i].long_name;
                                                    }
                                                }
                                                this.farmName = ""
                                                this.apt = ""
                                                this.streetNumber = ""
                                                let mapvalue: any[] = [{ "long": this.longitude, "lat": this.latitude, city: this.city, state: this.state, country: this.country, zipCode: this.zip, farmName: this.farmName, apt: this.apt, street: this.streetNumber }];
                                                let control = <FormArray>this.createPostForm.controls['values']
                                                let fg = <FormGroup>control.controls[this.thisindex]
                                                fg.controls['options'].setValue(mapvalue)
                                            } else {

                                                let mapvalue: any[] = [{ "long": this.longitude, "lat": this.latitude }];
                                                let control = <FormArray>this.createPostForm.controls['values']
                                                let fg = <FormGroup>control.controls[this.thisindex]
                                                fg.controls['options'].setValue(mapvalue)
                                            }
                                        });

                                });
                            });

                        }
                    })
                }
                else {
                    this.novalues = true
                    console.log("Preference Not Found")
                    this.noprefsetting = true;
                    let control = <FormArray>this.createPostForm.controls['values']
                    console.log(control.controls)
                    let length = control.length
                    console.log(length)
                    while (length--) {
                        console.log("removing control")
                        control.removeAt(length)
                        console.log(control.controls)
                    }
                }
            });
    }
    farmData(data) {
        return data.farmName + ",  " + data.street + ",  " + data.city + ",  " + data.state + ",  " + data.country + " - " + data.zipCode;
    }
    selectFarmData(e) {
        if (e) {
            var data = JSON.parse(e)
            this.farmName = data.farmName
            this.farmId = data.farmID
            this.apt = data.apt
            this.streetNumber = data.street
            this.zip = data.zipCode
            this.city = data.city
            this.state = data.state
            this.country = data.country
            this.latitude = data.lat  // for change map
            this.longitude = data.long // for change map

        }
    }
    initAddPostsettingForm() {
        this.createPostForm = this.fb.group({
            "userId": [this.selecteduser.userId, Validators.compose([
                Validators.required])],
            "productType": ['', Validators.compose([
                Validators.required])],
            "productId": ['', Validators.compose([
                Validators.required])],
            "values": this.fb.array([])
        })
    }
    cropped(e, i) {
        console.log(this.data)
    }
    confirmUpload(i) {
        console.log(this.data.image)
        let dataURI = this.data.image;
        function dataURItoBlob(dataURI: string) {
            // convert base64 to raw binary data held in a string
            // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
            var byteString = atob(dataURI.split(',')[1]);

            // separate out the mime component
            var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]

            // write the bytes of the string to an ArrayBuffer
            var ab = new ArrayBuffer(byteString.length);
            var ia = new Uint8Array(ab);
            for (var i = 0; i < byteString.length; i++) {
                ia[i] = byteString.charCodeAt(i);
            }

            // write the ArrayBuffer to a blob, and you're done
            var blob = new Blob([ab], { type: mimeString });
            return blob;

            // Old code
            // var bb = new BlobBuilder();
            // bb.append(ab);
            // return bb.getBlob(mimeString);
        }
        var blob = dataURItoBlob(dataURI);
        var fileName = (Math.random().toString(36).substring(7) + ".png");
        let imgagefile = new File([blob], fileName)
        console.log(blob)
        this.makeFileRequest(URL, [], imgagefile).then((result) => {
            console.log("result['url']", JSON.stringify(result['url']));
            const url = result['url'];
            const option = <FormArray>this.createPostForm.controls['values']
            const name = <FormGroup>option.controls[i]
            let options = name.controls['options']
            options.setValue([{ imageUrl: API_URL+"/" + url, type: "0" }])
            console.log(options)
            this.imageurl[i] = API_URL+"/" + url;

            console.log(this.imageurl[i]);

            this.uploaded = true
        })
    }
    initOptions() {
        // return this.fb.group({
        //     'title': ['', Validators.compose([
        //         Validators.required])]
        // })
        // let title = new FormControl('')
        // return title;
        return this.fb.control('', Validators.required)
    }

    addOptions() {
        const control = <FormArray>this.createPostForm.controls['optionsValue'];
        control.push(this.initOptions());
    }

    deleteOptions(i: number) {
        const control = <FormArray>this.createPostForm.controls['optionsValue'];
        control.removeAt(i)
    }
    createPost() {
        if (!this.streetNumber || this.streetNumber == "" || !this.farmName || this.farmName == "" || this.city == "" || !this.city || !this.state || this.state == "" || !this.country || this.country == "" || !this.zip || this.zip == "") {
            // alert('Mandatory fields missing in location')
            this.mandatoryDataMissing = true
            setTimeout(function () {
                this.mandatoryDataMissing = false
            }, 3000);
        } else {
            this.mandatoryDataMissing = false
            let mapvalue: any[] = [{ "farmID": this.farmId?this.farmId:getRandom(16), "long": this.longitude.toString(), "lat": this.latitude.toString(), city: this.city, state: this.state, country: this.country, zipCode: this.zip, farmName: this.farmName, street: this.streetNumber, apt: this.apt }];
            let control = <FormArray>this.createPostForm.controls['values']
            let fg = <FormGroup>control.controls[this.thisindex]
            fg.controls['options'].setValue(mapvalue)
            this.createPostForm.value.farmDetails = mapvalue[0]   // added to push in users location object
          //  console.log("sdsadsada ", JSON.stringify(this.createPostForm.value.values));
            let values: any[] = []
            values = this.createPostForm.value.values;
            values.forEach((data, i) => {
                if (typeof data.options == 'string') {
                    this.createPostForm.value.values[i].options = [data.options]
                }
                else {
                    this.createPostForm.value.values[i].options = data.options
                }
            })
            console.log("this.createPostForm.value ", JSON.stringify(this.createPostForm.value));
            this.postsService.addPost(this.createPostForm.value)
                .subscribe((data) => {
                    if (data.errCode == 0) {
                        this.router.navigate(['pages/posts/inactive'])
                    }
                    else {
                        alert(data.Message);
                    }
                })
        }
    }
    setMandatory(e) {
        if (e.target.checked) {
            this.createPostForm.controls['mandatory'].setValue(1);
        }
        else {
            this.createPostForm.controls['mandatory'].setValue(0);

        }
    }
    setOCS(e) {
        if (e.target.checked) {
            this.createPostForm.controls['onCreateScreen'].setValue(1);
        }
        else {
            this.createPostForm.controls['onCreateScreen'].setValue(0);
        }

    }
    checkMap(e) {
        console.log(e);

    }
    createPref() {
        console.log(this.createPostForm.value)
    }

    setRadioValue(e, i) {
        const option = <FormArray>this.createPostForm.controls['values']
        const name = <FormGroup>option.controls[i]
        const options = name.controls['options']
        console.log(name)
        console.log(i)
        console.log(e.target.value)
        options.setValue([e.target.value])
        console.log(options.value)

    }

    setCheckValue(e, i, opt) {
        const option = <FormArray>this.createPostForm.controls['values']
        const name = <FormGroup>option.controls[i]
        const options = name.controls['options']
        if (e.target.checked) {
            console.log("checked")
            if (options.value) {
                this.values = options.value
                this.values.push(opt)
            }
            else {
                this.values.push(opt)
            }
            options.setValue(this.values)
        }
        else {
            console.log("unchecked")
            if (options.value) {
                this.values = options.value
                this.values.splice(this.values.indexOf(opt), 1)
            }
            // else {
            //     this.values.splice(this.values.indexOf(opt), 1)
            // }
            options.setValue(this.values)
        }
        console.log(options.value)
        this.values = []
    }
    setSaleValue(e, i, opt) {
        console.log(e)
        this.price[0] = e
        const option = <FormArray>this.createPostForm.controls['values']
        const name = <FormGroup>option.controls[i]
        const options = name.controls['options']
        options.setValue(this.price);
        console.log("Slider value " + options.value)
    }
    setLeaseValue(e, i, opt) {
        this.price[1] = e
        const option = <FormArray>this.createPostForm.controls['values']
        const name = <FormGroup>option.controls[i]
        const options = name.controls['options']
        options.setValue(this.price);
        console.log("Slider value " + options.value)
    }

    filesToUpload: Array<File>;

    upload(i, j) {
        this.makeFileRequest(URL, [], this.filesToUpload[0]).then((result) => {
            console.log(JSON.stringify(result['url']));
            const url = result['url'];
            const option = <FormArray>this.createPostForm.controls['values']
            const name = <FormGroup>option.controls[i]
            let options = name.controls['options']
            options.setValue([{ imageUrl: API_URL+":8009/" + url, type: "0" }])
            console.log(options)
            // for (var index = 0; index < options.length; index++) {
            //     console.log("options "+ JSON.stringify(options[index]))
            //     if(options[index].imageUrl == j && options[index].type == 1){
            //        options[index].imageUrl = API_URL+":8009/" + url
            //        options[index].thumbnailUrl = API_URL+":8009/" + url+".png"
            //     }
            //     else if(options[index].imageUrl == j && options[index].type == 0){
            //        options[index].imageUrl = API_URL+":8009/" + url
            //     }

            // }
            this.imageurl[i] = API_URL+":8009/" + url;
            console.log(this.imageurl)
        },
            (error) => {
                console.error(error);
            });
    }

    fileChangeEvent(fileInput: any, i, j) {
        this.filesToUpload = <Array<File>>fileInput.target.files;
        this.upload(i, j);
        const name = this.filesToUpload[0]
        console.log(this.filesToUpload[0])
    }

    makeFileRequest(url: string, params: Array<string>, files: any) {
        return new Promise((resolve, reject) => {
            var formData: any = new FormData();
            var xhr = new XMLHttpRequest();
            // for (var i = 0; i < files.length; i++) {
            //     formData.append("uploads[]", files[i], files[i].name);
            //     console.log(formData.value)
            // }
            formData.append("photo", files, files.name)
            console.log(formData)
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4) {
                    if (xhr.status == 200) {
                        resolve(JSON.parse(xhr.response));
                    } else {
                        reject(xhr.response);
                    }
                }
            }
            xhr.open("POST", url, true);
            xhr.send(formData);
        });
    }
    fileChange(e) {
        console.log(this.uploader)
    }

    onChange() {
        console.log("changed")
    }
}

function getRandom(length) {

    return Math.floor(Math.pow(10, length - 1) + Math.random() * 9 * Math.pow(10, length - 1)).toString();

}
