import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class PostsService {
    private Url: string;
    private headers = new Headers({
        'Content-Type': 'application/json',
        "authorization": 'Basic ' + btoa("horse" + ":" + "123456"),
        "x-access-token": localStorage.getItem("user")
    })
    private options = new RequestOptions({
        headers: this.headers
    })
    constructor(private http: Http) {

    }
    getAllActivePosts(p) {
        let body = JSON.stringify({pageNum: p});
        let options = new RequestOptions({ headers: this.headers });
        this.Url = API_URL+":2020/getAllActivePosts";
        return this.http.post(this.Url, body, options)
            .map(res => res.json());
    }
    makeActivePost(id) {
        let body = JSON.stringify({ _ids: id });
        let options = new RequestOptions({ headers: this.headers });
        this.Url = API_URL+":2020/makeActivePost";
        return this.http.post(this.Url, body, options)
            .map(res => res.json());
    }
    makeInactivePost(id) {
        let body = JSON.stringify({ _ids: id });
        let options = new RequestOptions({ headers: this.headers });
        this.Url = API_URL+":2020/makeInactivePost";
        return this.http.post(this.Url, body, options)
            .map(res => res.json());
    }

    getAllInActivePosts(p) {
        let body = JSON.stringify({pageNum: p});
        let options = new RequestOptions({ headers: this.headers });
        this.Url = API_URL+":2020/getAllInActivePosts";
        return this.http.post(this.Url, body, options)
            .map(res => res.json());
    }



    getAllIncomletePosts(p) {
        let body = JSON.stringify({pageNum: p});
        let options = new RequestOptions({ headers: this.headers });
        this.Url = API_URL+":2020/getAllIncompletePost";
        return this.http.post(this.Url, body, options)
            .map(res => res.json());
    }

    addPost(PostToAdd) 
    {
        // var values = [];
        for(let i=0; i < PostToAdd.values.length; i++)
        {
            if(PostToAdd.values[i]['options'] == "")
            {
                // PostToAdd.values.splice(i);
                delete PostToAdd.values[i]['options']; // yun add
            }
        }
        console.log(JSON.stringify(PostToAdd));
        let body = JSON.stringify(PostToAdd);
        this.Url = API_URL+":2020/createPost";
        return this.http.post(this.Url, body, this.options)
            .map(res => res.json());
    }

    deletePost(PostId) {
        let options = new RequestOptions({ headers: this.headers });
        let body = JSON.stringify({ _ids: PostId });
        let Url = API_URL+":2020/deletePost";
        return this.http.post(Url, body, options)
            .map(res => res.json());
    }
    editPost(Post) {
        let options = new RequestOptions({ headers: this.headers });
        let body = JSON.stringify(Post);
        this.Url = API_URL+":2020/editPosts";
        return this.http.post(this.Url, body, options)
            .map(res => res.json())

    }
    searchActivePost(seachControl) {
        let options = new RequestOptions({ headers: this.headers });
        let body = JSON.stringify({ searchP: seachControl });
        this.Url = API_URL+":2020/searchActivePost";
        return this.http.post(this.Url, body, options)
            .map(res => res.json())
    }
    searchInActivePost(seachControl) {
        let options = new RequestOptions({ headers: this.headers });
        let body = JSON.stringify({ searchP: seachControl });
        this.Url = API_URL+":2020/searchInActivePost";
        return this.http.post(this.Url, body, options)
            .map(res => res.json())
    }
    searchIncompletePost(seachControl) {
        let options = new RequestOptions({ headers: this.headers });
        let body = JSON.stringify({ searchP: seachControl });
        this.Url = API_URL+":2020/searchIncompletePost";
        return this.http.post(this.Url, body, options)
            .map(res => res.json())
    }

    getProducttypes() {
        let body = JSON.stringify({});
        this.Url = API_URL+":2020/getProducttypes";
        return this.http.post(this.Url, body, this.options)
            .map(res => res.json());
    }
    getPostByIdToView(post) {
        let body = JSON.stringify({ _id: post });
        this.Url = API_URL+":2020/getAdminPostByIdToView";
        return this.http.post(this.Url, body, this.options)
            .map(res => res.json());
    }
    getUserById(userId) {
        let body = JSON.stringify({ userId: userId });
        this.Url = API_URL+":2020/getUserById";
        return this.http.post(this.Url, body, this.options)
            .map(res => res.json());
    }
    getUsers() {
        let body = JSON.stringify({});
        let options = new RequestOptions({ headers: this.headers });
        this.Url = API_URL+":2020/getUser";
        return this.http.post(this.Url, body, options)
            .map(res => res.json());
    }

    getPostByUser(userId) {
        let body = JSON.stringify({ userId: userId });
        let options = new RequestOptions({ headers: this.headers });
        this.Url = API_URL+":2020/getPostByUser";
        return this.http.post(this.Url, body, options)
            .map(res => res.json());
    }

    getPostByUsers(userIds) {
        let body = JSON.stringify({ postIds: userIds });
        let options = new RequestOptions({ headers: this.headers });
        this.Url = API_URL+":2020/getPostByProductIds";
        return this.http.post(this.Url, body, options)
            .map(res => res.json());
    }
    searchUser(seachControl) {
        let options = new RequestOptions({ headers: this.headers });
        let body = JSON.stringify({ searchP: seachControl });
        this.Url = API_URL+":2020/searchUser";
        return this.http.post(this.Url, body, options)
            .map(res => res.json())
    }
    getTotalViewByPostId(post) {
        let body = JSON.stringify({ postId: post });
        this.Url = API_URL+":2020/getTotalViewByPostId";
        return this.http.post(this.Url, body, this.options)
            .map(res => res.json());
    }
    getWishListByPostId(post) {
        let body = JSON.stringify({ postId: post });
        this.Url = API_URL+":2020/getWishListByPostId";
        return this.http.post(this.Url, body, this.options)
            .map(res => res.json());
    }
    getUniqueViewByPostId(post) {
        let body = JSON.stringify({ postId: post });
        this.Url = API_URL+":2020/getUniqueViewByPostId";
        return this.http.post(this.Url, body, this.options)
            .map(res => res.json());
    }
    sendNotification(message, userIds) {
        let body = JSON.stringify({ mess: message.message, userId: userIds });
        this.Url = API_URL+":2020/sendNotificationToSelectedUsers";
        return this.http.post(this.Url, body, this.options)
            .map(res => res.json());
    }
          getMapData(lat,lng) {
             let GEOCODING = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' + lat + '%2C' + lng + '&language=en&key=AIzaSyCQD6uL6gTjHtlvcSpXlynpcHObpfI_HeE';
         
        //  let Url  =  API_URL +"/dashboard";
         //let UrlWithId= Url + id;
        return this.http.get(GEOCODING)
            .map(res => res.json());
    }
}
