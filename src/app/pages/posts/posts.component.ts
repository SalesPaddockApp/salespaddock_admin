import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { BaThemeSpinner, BaThemePreloader } from '../../theme/services';
import { Location } from '@angular/common';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import { PostsService } from './posts.service';
import { AuthService } from '../../sharedServices/authService/auth.service';

@Component({
  selector: 'posts',
  encapsulation: ViewEncapsulation.None,
  template: `
    <router-outlet></router-outlet>
    `,
  styles: [require('./posts.scss')],
})
export class Posts {
  constructor() {
  }

  //     <div class="row">
  //   <div class="col-md-12">
	// 	<button type="button" routerLink="./active" class="btn btn-primary" routerLinkActive="active">Active Post</button>
	// 	<button type="button" routerLink="./inactive" class="btn btn-primary" routerLinkActive="active">InActive Post</button>
	// 	<button type="button" routerLink="./create" class="btn btn-primary" routerLinkActive="active">Create Post</button>
	// </div>
  //   </div> 

}
