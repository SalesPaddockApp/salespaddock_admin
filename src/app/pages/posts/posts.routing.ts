import { Routes, RouterModule } from '@angular/router';

import { Posts } from './posts.component';
import { CreatePostComponent } from './createpost/createpost.component';
import { ActivePost} from './active/active.component';
import { InActivePost} from './inactive/inactive.component';
import { IncompletePost} from './incompletePost/incomplete.component';
import { EditPostComponent } from './editpost/editpost.component';
import { ViewPostComponent } from './view/view.component';
import { Total } from './total/total.component';
import { Wish } from './wish/wish.component';
import { Unique } from './unique/unique.component';
const routes: Routes = [
  {
    path: '',
    component: Posts,
    children: [
      { path: '', redirectTo: 'active', pathMatch: 'full' },
      { path: 'create', component: CreatePostComponent },
      { path: 'active', component: ActivePost },
      { path: 'inactive', component: InActivePost },
      { path: 'incomplete', component: IncompletePost }
    ]
  },
  { path: 'view/:id', component: ViewPostComponent },
  { path: 'edit/:id', component: EditPostComponent },
  { path: 'wishlist/:id', component: Wish },
  { path: 'uniqueview/:id', component: Unique },
  { path: 'totalview/:id', component: Total }
];

export const routing = RouterModule.forChild(routes);
