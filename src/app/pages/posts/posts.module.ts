import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgaModule } from '../../theme/nga.module';
import { ReactiveFormsModule } from '@angular/forms';
import { routing }       from './posts.routing';
import { Posts } from './posts.component';
import { PostsService } from './posts.service';
import { CreatePostComponent } from './createpost/createpost.component';
import { EditPostComponent } from './editpost/editpost.component';
import { ViewPostComponent } from './view/view.component';
import { Total } from './total/total.component';
import { Wish } from './wish/wish.component';
import { Unique } from './unique/unique.component';
import { PagesModule } from '../pages.module';
import { PrefsettingsService } from '../producttypes/prefsettings/prefsettings.service';
import { ActivePost} from './active/active.component';
import { InActivePost} from './inactive/inactive.component';
import { IncompletePost} from './incompletePost/incomplete.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgaModule,
    routing,
    ReactiveFormsModule,
    PagesModule
  ],
  declarations: [
    Posts,
    CreatePostComponent,
    EditPostComponent,
    ActivePost,
    InActivePost,
    IncompletePost,
    ViewPostComponent,
    Total,
    Unique,
    Wish
  ],
  providers: [
    PostsService,
    PrefsettingsService
  ]
})
export default class postsModule {}
