import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';

import { routing }       from './pages.routing';
import { NgaModule } from '../theme/nga.module';

import { Pages } from './pages.component';
import { SocketService } from '../sharedServices/socketService/socket.service';
import { AuthService } from '../sharedServices/authService/auth.service';
import { OrderBy } from '../sharedServices/pipes/orderByPipe.ts';
import { ChekedFilterPipe } from '../sharedServices/pipes/checkedPipe.ts'
import { FileUploadModule } from 'ng2-file-upload';
import { AgmCoreModule } from "angular2-google-maps/core";
import { MdSliderModule } from "@angular2-material/slider";
import { MdCheckbox } from "@angular2-material/checkbox";
import { MdRadioModule } from '@angular2-material/radio';
import {ImageCropperComponent, CropperSettings} from 'ng2-img-cropper';
import { DatePickerModule } from 'ng2-datepicker';
import { MyDatePickerModule } from 'mydatepicker';
import {SelectModule} from 'ng2-select/ng2-select';
import { CKEditorModule } from 'ng2-ckeditor';

@NgModule({
  imports: [CommonModule, NgaModule, routing,
    FileUploadModule,
    AgmCoreModule.forRoot({
      apiKey: "AIzaSyCQD6uL6gTjHtlvcSpXlynpcHObpfI_HeE",
      libraries: ["places"]
    }),
    MdSliderModule,MdRadioModule,DatePickerModule,MyDatePickerModule,SelectModule,CKEditorModule
    ],
  declarations: [Pages, OrderBy,
    MdCheckbox,ChekedFilterPipe,ImageCropperComponent],
  exports: [OrderBy,
    FileUploadModule,
    AgmCoreModule,
    MdSliderModule,
    MdCheckbox,
    MdRadioModule,
    ChekedFilterPipe,
    ImageCropperComponent,DatePickerModule,MyDatePickerModule,SelectModule,CKEditorModule
    ],
  providers: [AuthService, SocketService]
})
export class PagesModule {
}
