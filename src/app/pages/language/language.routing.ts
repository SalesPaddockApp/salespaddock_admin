import { Routes, RouterModule }  from '@angular/router';

import { Language } from './language.component';
const routes: Routes = [
  {
    path: '',
    component: Language,
    children: [
    ]}
];

export const routing = RouterModule.forChild(routes);
