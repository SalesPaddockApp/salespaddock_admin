import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgaModule } from '../../theme/nga.module';
import { ReactiveFormsModule } from '@angular/forms';
import { routing }       from './language.routing';
import { Language } from './language.component';
import { LanguageService } from './language.service';
import { PagesModule } from '../pages.module';
import { PostsService } from '../posts/posts.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgaModule,
    routing,
    ReactiveFormsModule,
    PagesModule
  ],
  declarations: [
    Language
  ],
  providers: [
    LanguageService,
    PostsService
  ]
})
export default class LanguageModule {}
