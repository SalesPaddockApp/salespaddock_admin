import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class LanguageService {
    private Url: string;
    private headers = new Headers({
        'Content-Type': 'application/json',
        "authorization": 'Basic ' + btoa("horse" + ":" + "123456"),
        "x-access-token": localStorage.getItem("user")
    })
    private options = new RequestOptions({
        headers: this.headers
    })
    constructor(private http: Http) {

    }
    getLanguage() {
        let body = JSON.stringify({});
        let options = new RequestOptions({ headers: this.headers });
        this.Url = API_URL+":2020/getLanguage";
        return this.http.post(this.Url, body, options)
            .map(res => res.json());
    }
    addLanguage(LanguageToAdd) {
        // let options = new RequestOptions({ headers: this.headers });
        let body = JSON.stringify(LanguageToAdd);
        this.Url = API_URL+":2020/createLanguage";
        return this.http.post(this.Url, body, this.options)
            .map(res => res.json());
    }

    deleteLanguage(LanguageId) {
        let options = new RequestOptions({ headers: this.headers });
        let body = JSON.stringify({ _ids: LanguageId });
        let Url = API_URL+":2020/deleteLanguage";
        return this.http.post(Url, body, options)
            .map(res => res.json());
    }
    editLanguage(Language) {
        let options = new RequestOptions({ headers: this.headers });
        let body = JSON.stringify(Language);
        this.Url = API_URL+":2020/editLanguage";
        return this.http.post(this.Url, body, options)
            .map(res => res.json())

    }
    search(seachControl) {
        let options = new RequestOptions({ headers: this.headers });
        let body = JSON.stringify({ searchP: seachControl });
        this.Url = API_URL+":2020/searchLanguage";
        return this.http.post(this.Url, body, options)
            .map(res => res.json())
    }
    getLanguageBy_Id(id) {
        let body = JSON.stringify({ _id: id });
        this.Url = API_URL+":2020/getLanguageBy_Id";
        return this.http.post(this.Url, body, this.options)
            .map(res => res.json());
    }
    makeActive(id) {
        let body = JSON.stringify({ _id: id });
        this.Url = API_URL+":2020/makeLanguageActive";
        return this.http.post(this.Url, body, this.options)
            .map(res => res.json());
    }
    makeInactive(id) {
        let body = JSON.stringify({ _id: id });
        this.Url = API_URL+":2020/makeLanguageInactive";
        return this.http.post(this.Url, body, this.options)
            .map(res => res.json());
    }
    getDeviceLog(id) {
        let body = JSON.stringify({ _id: id });
        this.Url = API_URL+":2020/getDeviceLog";
        return this.http.post(this.Url, body, this.options)
            .map(res => res.json());
    }
}
