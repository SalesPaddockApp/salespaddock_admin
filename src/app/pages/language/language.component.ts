import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { BaThemeSpinner, BaThemePreloader } from '../theme/services';
import { Location } from '@angular/common';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import { LanguageService } from './language.service';
import { AuthService } from '../../sharedServices/authService/auth.service';
import { FileUploader } from 'ng2-file-upload';
const URL = API_URL+':8009/upload';

@Component({
  selector: 'language',
  encapsulation: ViewEncapsulation.None,
  template: require('./language.html'),
  styles: [require('./language.scss')]
})
export class Language implements OnInit {
  private Imgage: any;
  private imageurl: any;
  private editimageurl: any;
  private addlanguageForm: FormGroup;
  private editlanguageForm: FormGroup;
  private languageById: any;
  language: any;
  private nodata: boolean = false
  searchControl = new FormControl();
  private device: any;
  private ids: any[] = [];
  private lang: any;

  constructor(private _location: Location, private languageService: LanguageService, private fb: FormBuilder,
    private authService: AuthService, private router: Router) {
  }

  ngOnInit() {
    let _self = this;
    this.authService.checkCredentials();
    this.getLanguage();

    this.searchControl.valueChanges
      .debounceTime(400)
      .distinctUntilChanged()
      .flatMap(seachControl => this.languageService.search(seachControl))
      .subscribe(data => {
        if (data.errCode == 0) {
          this.language = data.response.data
        }
        else {
          this.languageService.getLanguage()
            .subscribe((res) => {
              if (res.errCode == 0) {
                this.nodata = false
                this.language = res.response.data;
              }
              else {
                this.nodata = true
              }
            });
        }
      });
    this.uploader.onSuccessItem = function (item: any, response: any, status: number, headers: any) {
      _self.Imgage = JSON.parse(response);
      _self.imageurl = (API_URL+":8009/" + _self.Imgage.url)
      console.log(_self.imageurl);
      if (_self.addlanguageForm) {
        if (_self.editlanguageForm) {
          _self.editlanguageForm.controls['img'].setValue(_self.imageurl)
        }
        _self.addlanguageForm.controls['img'].setValue(_self.imageurl)
      }
      else if (_self.editlanguageForm) {
        _self.editlanguageForm.controls['img'].setValue(_self.imageurl)
        if (_self.addlanguageForm) {
          _self.addlanguageForm.controls['img'].setValue(_self.imageurl)
        }
      }
      return { item, response, status, headers };
    };
    this.initAddlanguageForm()
  }

  public uploader: FileUploader = new FileUploader({ url: URL, autoUpload: true, itemAlias: "photo" });
  public hasBaseDropZoneOver: boolean = false;
  public hasAnotherDropZoneOver: boolean = false;

  public fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }

  public fileOverAnother(e: any): void {
    this.hasAnotherDropZoneOver = e;
  }


  initAddlanguageForm() {
    this.addlanguageForm = this.fb.group({
      "country": ['', Validators.compose([
        Validators.required])],
      "language": ['', Validators.compose([
        Validators.required])]
    })
  }
  getLanguageById() {
    this.languageById = this.lang;
    this.editlanguageForm = this.fb.group({
      "_id": this.languageById._id,
      "country": [this.languageById.country, Validators.compose([Validators.required])],
      "language": [this.languageById.language, Validators.compose([Validators.required])]
    })
  }

  getLanguage() {
    this.languageService.getLanguage()
      .subscribe((res) => {
        console.log(res)
        if (res.errCode == 0) {
          this.nodata = false
          this.language = res.response.data;
        }
        else {
          this.nodata = true
        }
      });
  }

  addLanguage() {
    this.languageService.addLanguage(this.addlanguageForm.value)
      .subscribe((data) => {
        if (data.errCode == 0) {
          this.languageService.getLanguage()
            .subscribe(data => {
              if (data.errCode == 0) {
                this.nodata = false
                this.language = data.response.data;
              }
              else {
                this.nodata = true
              }
            });
        }
      })
  }

  editLanguage() {
    this.languageService.editLanguage(this.editlanguageForm.value)
      .subscribe((data) => {
        if (data.errCode == 0) {
          this.ids = []
          this.languageService.getLanguage()
            .subscribe(data => {
              if (data.errCode == 0) {
                this.nodata = false
                this.language = data.response.data;
              }
              else {
                this.nodata = true
              }
            });
        }
      })
  }

  deleteLanguage() {
    this.languageService.deleteLanguage(this.ids)
      .subscribe((data) => {
        console.log(data)
        if (data.errCode == 0) {
          this.language = null;
          this.ids = []
          this.languageService.getLanguage()
            .subscribe(data => {
              if (data.errCode == 0) {
                this.nodata = false
                this.language = data.response.data;
              }
              else {
                this.nodata = true
              }
            });
        }
      })
  }

  setIds(e, producttype) {
    if (e.target.checked) {
      this.ids.push(producttype._id)
      this.lang = producttype
      this.getLanguageById()
    }
    else {
      this.ids.splice(this.ids.indexOf(producttype._id));
    }
    console.log(this.ids)
  }
}
