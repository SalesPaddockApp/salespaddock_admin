import { Routes, RouterModule } from '@angular/router';
import { Pages } from './pages.component';
// noinspection TypeScriptValidateTypes
const routes: Routes = [
  {
    path: 'login',
    loadChildren: () => System.import('../login/login.module')
  },
  {
    path: 'pages',
    component: Pages,
    children: [
      { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
      { path: 'dashboard', loadChildren: () => System.import('./dashboard/dashboard.module') },
      { path: 'producttypes', loadChildren: () => System.import('./producttypes/producttypes.module') },
      { path: 'users', loadChildren: () => System.import('./users/users.module') },
      { path: 'posts', loadChildren: () => System.import('./posts/posts.module') },
      { path: 'shows', loadChildren: () => System.import('./shows/shows.module') },
      { path: 'currency', loadChildren: () => System.import('./currency/currency.module') },
      { path: 'language', loadChildren: () => System.import('./language/language.module') },
      { path: 'faq', loadChildren: () => System.import('./faq/faq.module') }
    ]
  }
];

export const routing = RouterModule.forChild(routes);
