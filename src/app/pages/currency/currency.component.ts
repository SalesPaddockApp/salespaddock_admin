import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { BaThemeSpinner, BaThemePreloader } from '../theme/services';
import { Location } from '@angular/common';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import { CurrencyService } from './currency.service';
import { AuthService } from '../../sharedServices/authService/auth.service';
import { FileUploader } from 'ng2-file-upload';
const URL = API_URL+':8009/upload';

@Component({
  selector: 'currency',
  encapsulation: ViewEncapsulation.None,
  template: require('./currency.html'),
  styles: [require('./currency.scss')]
})
export class Currency implements OnInit {
  private Imgage: any;
  private imageurl: any;
  private editimageurl: any;
  private addcurrencyForm: FormGroup;
  private editcurrencyForm: FormGroup;
  private currencyById: any;
  currency: any;
  private nodata: boolean = false
  searchControl = new FormControl();
  private device: any;
  private ids: any[] = [];
  private curr: any;

  constructor(private _location: Location, private currencyService: CurrencyService, private fb: FormBuilder,
    private authService: AuthService, private router: Router) {
  }

  ngOnInit() {
    let _self = this;
    this.authService.checkCredentials();
    this.getCurrency();

    this.searchControl.valueChanges
      .debounceTime(400)
      .distinctUntilChanged()
      .flatMap(seachControl => this.currencyService.search(seachControl))
      .subscribe(data => {
        if (data.errCode == 0) {
          this.currency = data.response.data
        }
        else {
          this.currencyService.getCurrency()
            .subscribe((res) => {
              if (res.errCode == 0) {
                this.nodata = false
                this.currency = res.response.data;
              }
              else {
                this.nodata = true
              }
            });
        }
      });
    this.uploader.onSuccessItem = function (item: any, response: any, status: number, headers: any) {
      _self.Imgage = JSON.parse(response);
      _self.imageurl = (API_URL+":8009/" + _self.Imgage.url)
      console.log(_self.imageurl);
      if (_self.addcurrencyForm) {
        if (_self.editcurrencyForm) {
          _self.editcurrencyForm.controls['img'].setValue(_self.imageurl)
        }
        _self.addcurrencyForm.controls['img'].setValue(_self.imageurl)
      }
      else if (_self.editcurrencyForm) {
        _self.editcurrencyForm.controls['img'].setValue(_self.imageurl)
        if (_self.addcurrencyForm) {
          _self.addcurrencyForm.controls['img'].setValue(_self.imageurl)
        }
      }
      return { item, response, status, headers };
    };
    this.initAddcurrencyForm()
  }

  public uploader: FileUploader = new FileUploader({ url: URL, autoUpload: true, itemAlias: "photo" });
  public hasBaseDropZoneOver: boolean = false;
  public hasAnotherDropZoneOver: boolean = false;

  public fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }

  public fileOverAnother(e: any): void {
    this.hasAnotherDropZoneOver = e;
  }


  initAddcurrencyForm() {
    this.addcurrencyForm = this.fb.group({
      "country": ['', Validators.compose([
        Validators.required])],
      "currency": ['', Validators.compose([
        Validators.required])],
      "symbol": ['', Validators.compose([
        Validators.required])]
    })
  }
  getcurrencyById() {
    this.currencyById = this.curr;
    this.editcurrencyForm = this.fb.group({
      "_id": this.currencyById._id,
      "country": [this.currencyById.country, Validators.compose([Validators.required])],
      "currency": [this.currencyById.currency, Validators.compose([Validators.required])],
      "symbol": [this.currencyById.symbol, Validators.compose([Validators.required])]
    })
  }

  getCurrency() {
    this.currencyService.getCurrency()
      .subscribe((res) => {
        console.log(res)
        if (res.errCode == 0) {
          this.nodata = false
          this.currency = res.response.data;
        }
        else {
          this.nodata = true
        }
      });
  }

  addCurrency() {
    this.currencyService.addCurrency(this.addcurrencyForm.value)
      .subscribe((data) => {
        if (data.errCode == 0) {
          this.currencyService.getCurrency()
            .subscribe(data => {
              if (data.errCode == 0) {
                this.nodata = false
                this.currency = data.response.data;
              }
              else {
                this.nodata = true
              }
            });
        }
      })
  }

  editCurrency() {
    this.currencyService.editCurrency(this.editcurrencyForm.value)
      .subscribe((data) => {
        if (data.errCode == 0) {
          this.currencyService.getCurrency()
            .subscribe(data => {
              if (data.errCode == 0) {
                this.nodata = false
                this.currency = data.response.data;
              }
              else {
                this.nodata = true
              }
            });
        }
      })
  }

  deleteCurrency() {
    this.currencyService.deleteCurrency(this.ids)
      .subscribe((data) => {
        console.log(data)
        if (data.errCode == 0) {
          this.currency = null;
          this.currencyService.getCurrency()
            .subscribe(data => {
              if (data.errCode == 0) {
                this.nodata = false
                this.currency = data.response.data;
              }
              else {
                this.nodata = true
              }
            });
        }
      })
  }

  setIds(e, producttype) {
    if (e.target.checked) {
      this.ids.push(producttype._id)
      this.curr = producttype
      this.getcurrencyById()
    }
    else {
      this.ids.splice(this.ids.indexOf(producttype._id));
    }
    console.log(this.ids)
  }
}
