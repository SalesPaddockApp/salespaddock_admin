import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class CurrencyService {
    private Url: string;
    private headers = new Headers({
        'Content-Type': 'application/json',
        "authorization": 'Basic ' + btoa("horse" + ":" + "123456"),
        "x-access-token": localStorage.getItem("user")
    })
    private options = new RequestOptions({
        headers: this.headers
    })
    constructor(private http: Http) {

    }
    getCurrency() {
        let body = JSON.stringify({});
        let options = new RequestOptions({ headers: this.headers });
        this.Url = API_URL+":2020/getCurrency";
        return this.http.post(this.Url, body, options)
            .map(res => res.json());
    }
    addCurrency(CurrencyToAdd) {
        // let options = new RequestOptions({ headers: this.headers });
        let body = JSON.stringify(CurrencyToAdd);
        this.Url = API_URL+":2020/createCurrency";
        return this.http.post(this.Url, body, this.options)
            .map(res => res.json());
    }

    deleteCurrency(CurrencyId) {
        let options = new RequestOptions({ headers: this.headers });
        let body = JSON.stringify({ _ids: CurrencyId });
        let Url = API_URL+":2020/deleteCurrency";
        return this.http.post(Url, body, options)
            .map(res => res.json());
    }
    editCurrency(Currency) {
        let options = new RequestOptions({ headers: this.headers });
        let body = JSON.stringify(Currency);
        this.Url = API_URL+":2020/editCurrency";
        return this.http.post(this.Url, body, options)
            .map(res => res.json())

    }
    search(seachControl) {
        let options = new RequestOptions({ headers: this.headers });
        let body = JSON.stringify({ searchP: seachControl });
        this.Url = API_URL+":2020/searchCurrency";
        return this.http.post(this.Url, body, options)
            .map(res => res.json())
    }
    getCurrencyBy_Id(id) {
        let body = JSON.stringify({ _id: id });
        this.Url = API_URL+":2020/getCurrencyBy_Id";
        return this.http.post(this.Url, body, this.options)
            .map(res => res.json());
    }
    makeActive(id) {
        let body = JSON.stringify({ _id: id });
        this.Url = API_URL+":2020/makeCurrencyActive";
        return this.http.post(this.Url, body, this.options)
            .map(res => res.json());
    }
    makeInactive(id) {
        let body = JSON.stringify({ _id: id });
        this.Url = API_URL+":2020/makeCurrencyInactive";
        return this.http.post(this.Url, body, this.options)
            .map(res => res.json());
    }
    getDeviceLog(id) {
        let body = JSON.stringify({ _id: id });
        this.Url = API_URL+":2020/getDeviceLog";
        return this.http.post(this.Url, body, this.options)
            .map(res => res.json());
    }
}
