import { Routes, RouterModule }  from '@angular/router';

import { Currency } from './currency.component';
const routes: Routes = [
  {
    path: '',
    component: Currency,
    children: [
    ]}
];

export const routing = RouterModule.forChild(routes);
