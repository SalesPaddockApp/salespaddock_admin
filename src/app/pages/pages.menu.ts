export const PAGES_MENU = [
  {
    path: 'pages',
    children: [
      {
        path: 'dashboard',
        data: {
          menu: {
            title: 'Dashboard',
            icon: 'ion-android-home',
            selected: false,
            expanded: false,
            order: 0
          }
        }
      },
      {
        path: 'producttypes',
        data: {
          menu: {
            title: 'Product Types',
            icon: 'ion-briefcase',
            selected: false,
            expanded: false,
            order: 200,
          }
        }
      },
      {
        path: 'posts',
        data: {
          menu: {
            title: 'Posts',
            icon: 'ion-ios-cart',
            selected: false,
            expanded: false,
            order: 300,
          }
        },
          children: [
            {
              path: 'active',
              data: {
                menu: {
                  title: 'Active Posts',
                }
              }
            },
            {
              path: 'inactive',
              data: {
                menu: {
                  title: 'In-Active Posts',
                }
              }
            },
            {
              path: 'incomplete',
              data: {
                menu: {
                  title: 'In-complete Posts',
                }
              }
            },
            {
              path: 'create',
              data: {
                menu: {
                  title: 'Create Post',
                }
              }
            }
          ]
      },
      {
        path: 'shows',
        data: {
          menu: {
            title: 'Shows',
            icon: 'ion-ios-gear',
            selected: false,
            expanded: false,
            order: 300,
          }
        },
          children: [
            {
              path: 'active',
              data: {
                menu: {
                  title: 'Active Shows',
                }
              }
            },
            {
              path: 'upcoming',
              data: {
                menu: {
                  title: 'Upcoming Shows',
                }
              }
            },
            {
              path: 'archived',
              data: {
                menu: {
                  title: 'Archived Shows',
                }
              }
            },
            {
              path: 'inactive',
              data: {
                menu: {
                  title: 'In-Active Shows',
                }
              }
            }
          ]
      },
      {
        path: 'users',
        data: {
          menu: {
            title: 'Users',
            icon: 'ion-person',
            selected: false,
            expanded: false,
            order: 300,
          }
        },
          children: [
            {
              path: 'active',
              data: {
                menu: {
                  title: 'Active Users',
                }
              }
            },
            {
              path: 'inactive',
              data: {
                menu: {
                  title: 'In-Active Users',
                }
              }
            }
          ]
      },
      {
        path: 'currency',
        data: {
          menu: {
            title: 'Currency',
            icon: 'ion-cash',
            selected: false,
            expanded: false,
            order: 300,
          }
        },
          children: [
            
          ]
      },
      {
        path: 'language',
        data: {
          menu: {
            title: 'Language',
            icon: 'ion-earth',
            selected: false,
            expanded: false,
            order: 300,
          }
        },
          children: [
            
          ]
      },
      {
        path: 'faq',
        data: {
          menu: {
            title: 'FAQ',
            icon: 'ion-help-circled',
            selected: false,
            expanded: false,
            order: 300,
          }
        },
          children: [
            
          ]
      }
    ]
  }
];
