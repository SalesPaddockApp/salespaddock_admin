import { Pipe, PipeTransform } from '@angular/core';
import * as _ from "lodash";

@Pipe({ name: 'checked', pure: false })
export class ChekedFilterPipe implements PipeTransform {
    transform(value: any[], args: any[]): any {
        let checked: any[] = args
        let isChecked: boolean = false;
        return _.difference(value, checked);
    }
}