import { Injectable } from '@angular/core';
import { RouterModule, Router }   from '@angular/router';

import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';

export class User {
    constructor(
        public username: string,
        public password: string) { }
}

@Injectable()
export class AuthService {

    private _loginUrl = API_URL +':2020/api/adminLogin';
    private username:string = 'horse';
    private password:string = '123456';

    constructor(
        private _router: Router,
        private _http: Http) { }


    login(user: any) {

        let body = JSON.stringify(user);

        let headers = new Headers();
        let auth:string = 'Basic ' +btoa(this.username + ":" + this.password);
        headers.append('Content-Type', 'application/json');
        headers.append("authorization", auth);
        
        return this._http.post(this._loginUrl, body, { headers: headers })
            .map(res => res.json())
            // .catch(this.handleError);
    }

    private handleError(error: Response) {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }

    checkCredentials() {
        if (localStorage.getItem("user") === null) {
            this._router.navigate(['login']);
        }
    }
}
