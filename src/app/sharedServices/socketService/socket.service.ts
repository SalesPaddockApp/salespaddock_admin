import { Injectable } from '@angular/core';
import { Observer } from 'rxjs/Observer';
import { Observable } from 'rxjs/Observable';

import { io } from './socket.loader.ts';

@Injectable()
export class SocketService {

    private socket: any = null;

    notifications$: Observable<string>;
    private _notificationsObserver: Observer<string>;

    newUser$: Observable<string>;
    private _newUserObserver: Observer<string>;

    newTextMess$: Observable<string>;
    private _newTextObserver: Observer<string>;

    textMess$: Observable<string>;
    private _textMessObserver: Observer<string>;

    newGroupData$: Observable<string>;
    private _groupDataObserver: Observer<string>;

    constructor() {
        this.connectSocket();
        this.notifications$ = new Observable<string>((observer: any) => this._notificationsObserver = observer);
        this.newUser$ = new Observable<string>((observer: any) => this._newUserObserver = observer);
        this.getNewUser();
        this.newTextMess$ = new Observable<string>((observer: any) => this._newTextObserver = observer);

        this.textMess$ = new Observable<string>((observer: any) => this._textMessObserver = observer);

        this.newGroupData$ = new Observable<string>((observer: any) => this._groupDataObserver = observer);
    }

    connectSocket() {
        this.socket = io('http://35.161.211.180:9001');
        this.socket.on('connection', function (data: any) {
            console.log(data);
        });
    }

    getNotifications() {
        console.log('get notify');
        try {
            var notificationText: string;
            this.socket.on('messageAdmin', (data: any) => {
                data = { message: "new message is recieved." };
                this._notificationsObserver.next(data);
            })
        }
        catch (err) {
            console.log('error 1');
        }
    }

    getNewUser() {
        console.log("new user")
        let observable = new Observable(observer => {
            this.socket = io('http://35.161.211.180:9001');
            this.socket.on('newUser', (data) => {
                observer.next(data);
            });
            return () => {
                this.socket.disconnect();
            };
        })
        return observable;

        // try {
        //     this.socket.on('newUser', (data: any) => {
        //         this._newUserObserver.next(data);
        //     })
        // }
        // catch (err) {
        //     console.log('error 2');
        // }
    }

    // getNewText() {
    //     console.log('get new Text');
    //     try {
    //         this.socket.on('messageAdmin', (data: any) => {
    //             console.log(JSON.stringify(data));
    //             this._newTextObserver.next(data);
    //         })
    //     }
    //     catch (err) {
    //         console.log('error 2');
    //     }
    // }
    getNewText() {
        let observable = new Observable(observer => {
            this.socket = io('http://35.161.211.180:9001');
            this.socket.on('messageAdmin', (data) => {
                observer.next(data);
            });
            return () => {
                this.socket.disconnect();
            };
        })
        return observable;
    }
    getAllMessages() {
        let observable = new Observable(observer => {
            this.socket = io('http://35.161.211.180:9001');
            this.socket.on('messageForAdmin', (data) => {
                observer.next(data);
            });
            return () => {
                this.socket.disconnect();
            };
        })
        return observable;
    }

    getGroupChartData() {
        try {

            this.socket.on('grpDataAdmn', (data: any) => {
                // console.log(data);
                this._groupDataObserver.next(data);
            })
        }
        catch (err) {
            console.log("found error: " + err);
        }
    }
}